#include <stdio.h>
#include <string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declaration
	int MyStrlen(char[]);

	//Variable Declaration
	char strArray[10][15] = { "Hello!", "Welcome", "To", "Real", "Time", "Rendering", "Batch", "(2018-19-20)", "Of", "ASTROMEDICOMP." };
	int char_size, strArray_size, strArray_num_elements, strArray_num_rows, strArray_num_columns;
	int strActual_num_chars = 0, i;

	//Code
	char_size = sizeof(char);

	strArray_size = sizeof(strArray);
	printf("\nSize of Two Dimensional Character Array (String Array) = %d\n", strArray_size);

	strArray_num_rows = strArray_size / sizeof(strArray[0]);
	printf("\nNumber of Rows (Strings) in the Two Dimensional Character Array (String Array) = %d\n", strArray_num_rows);

	strArray_num_columns = sizeof(strArray[0]) / char_size;
	printf("\nNumber of Columns in the Two Dimensional Character Array (String Array) = %d\n", strArray_num_columns);

	strArray_num_elements = strArray_num_rows * strArray_num_columns;
	printf("\nMaximum number of Elements (Characters) in the Two Dimensional Character Array (String Array) = %d\n", strArray_num_elements);

	for (i = 0; i < strArray_num_rows; i++)
	{
		strActual_num_chars = strActual_num_chars + MyStrlen(strArray[i]);
	}
	printf("\nActual number of Elements (Characters) in the Two Dimensional Character Array (String Array) = %d\n", strActual_num_chars);

	printf("\nStrings in the 2D Array: \n");

	printf("%s ", strArray[0]);
	printf("%s ", strArray[1]);
	printf("%s ", strArray[2]);
	printf("%s ", strArray[3]);
	printf("%s ", strArray[4]);
	printf("%s ", strArray[5]);
	printf("%s ", strArray[6]);
	printf("%s ", strArray[7]);
	printf("%s ", strArray[8]);
	printf("%s\n\n", strArray[9]);

	return 0;
}


//Function for calculating string length
int MyStrlen(char str[])
{
	//Variable Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return(string_length);
}
