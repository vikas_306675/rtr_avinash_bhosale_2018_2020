#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	//Function declaraton
	void MyAddition(int, int);

	//Variable Declaration
	int a, b;

	//Code
	printf("\nEnter Integer Value for A: ");
	scanf("%d", &a);

	printf("\nEnter Integer Value for B: ");
	scanf("%d", &b);

	MyAddition(a, b);

	return 0;
}


//User Definied function with no return value and 2 parameters
void MyAddition(int a, int b)
{
	//Variable Declaraton
	int sum;

	//Code	
	sum = a + b;

	printf("\nSum of %d and %d = %d\n", a, b, sum);
}