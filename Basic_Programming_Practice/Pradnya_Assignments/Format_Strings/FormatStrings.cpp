#include<stdio.h>

int main(void)
{
	//code
	printf("\nHello World!! \n\n");

	int a = 13;
	printf("Integer decimal value of 'A': %d\n", a);
	printf("integer octal value of 'A': %o\n", a);
	printf("Hexadecimal value of 'A' (Hexadecimal letters in lower case): %x\n", a);
	printf("Hexadecimal value of 'A' (Hexadecimal letters in upper case): %X\n", a);

	char ch = 'A';
	printf("Character ch: %c\n", ch);

	char str[] = "Avinash Bhosale";
	printf("String ch: %s\n", str);

	long num = 12345678L;
	printf("Long integer: %ld\n", num);

	unsigned int b = 10;
	printf("Unsinged integer 'B': %u\n", b);

	float fnum = 3.14234f;
	printf("Floating point number with just %%f: %f\n", fnum);
	printf("Floating point number with %%4.2f: %4.2f\n", fnum);
	printf("Floating point number with %%2.5f: %2.5f\n", fnum);

	double pi = 3.14159265358979323846;
	printf("Double precision Floating point number without exponential: %g\n", pi);
	printf("Double precision Floating point number with exponential (lower case): %e\n", pi);
	printf("Double precision Floating point number with exponential (upper case): %E\n", pi);
	printf("Double hexadecimal value of 'pi' (Hexadecimal letters in lower case): %a\n", pi);
	printf("Double hexadecimal value of 'pi' (Hexadecimal letters in upper case): %A\n", pi);

	return 0;
}