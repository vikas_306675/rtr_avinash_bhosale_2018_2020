#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#define _USE_MATH_DEFINES 1
#include<math.h>

#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glu32.lib")

//Global Variable Declaration
HWND ghwnd;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
FILE *gbFile = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
int winWidth = 800;
int winHeight = 600;
bool gbDone = false;
bool gbFullscreen = false;
bool gbActive = false;
bool bLight = false;
GLfloat LightAmbientZero[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat LightDiffuseZero[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat LightSpecularZero[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat LightPositionZero[] = { 0.0f, 2.0f, 1.0f, 1.0f };
GLfloat LightAmbientOne[] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat LightDiffuseOne[] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat LightSpecularOne[] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat LightPositionOne[] = { 2.0f, 0.0f, 1.0f, 1.0f };
GLfloat LightAmbientTwo[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat LightDiffuseTwo[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat LightSpecularTwo[] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat LightPositionTwo[] = { -2.0f, 0.0f, 1.0f, 1.0f };
GLUquadric *quadric[20];
GLfloat translate_z = 10.0;
GLfloat z_rotation = 0.0;
typedef struct materialStruct
{
	GLfloat MaterialAmbient[4];
	GLfloat MaterialDiffuse[4];
	GLfloat MaterialSpecular[4];
	GLfloat MaterialShininess[1];

	struct materialStruct *next;
}mStruct;

mStruct *last = (mStruct *)malloc(sizeof(mStruct));

//Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable Declaration
	int iRet;
	HWND hwnd;
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");

	//Function Declaration
	int initialize(void);
	void display(void);
	void update(void);

	//Open the log file and Error Checking
	if (fopen_s(&gbFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gbFile, "Log File Created Successfully\n");
	}

	//Creating BluePrint for the window
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = szAppName;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;

	//Registering the window class
	RegisterClassEx(&wndclass);

	//Creating the Window in Memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("My OpenGL Window - Avinash Bhosale"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		winWidth,
		winHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Call to Initialize function to Initialize OpenGL
	iRet = initialize();

	//Error checking for Initialization of  OpenGL
	if (iRet == -1)
	{
		fprintf(gbFile, "Choosing Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gbFile, "Set Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gbFile, "Create Context Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gbFile, "Make Current Context Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gbFile, "Initialization of OpenGL is successfull\n");
	}

	//Showing the Window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (msg.message == WM_QUIT)
			{
				gbDone = true;
			}
			else
			{
				fprintf(gbFile, "Message = %d\n", msg.message);
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == true)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}

//CallBack Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void resize(int, int);
	void uninitialize(void);
	void FullscreenToggle(void);

	switch (iMsg)
	{

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			FullscreenToggle();
			break;
		}
		break;

		/*case WM_CHAR:
			switch (wParam)
			{
			case 'L':
			case 'l':
				if (bLight == false)
				{
					bLight = true;
					glEnable(GL_LIGHTING);
				}
				else
				{
					bLight = false;
					glDisable(GL_LIGHTING);
				}
				break;
			}
			break;*/

	case WM_ERASEBKGND:
		return(0);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_SETFOCUS:
		gbActive = true;
		break;

	case WM_KILLFOCUS:
		gbActive = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		winWidth = LOWORD(lParam);
		winHeight = HIWORD(lParam);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//Fullscreen Toggle Function
void FullscreenToggle(void)
{
	//Variable Decalaration
	MONITORINFO mi;

	//Code
	if (gbFullscreen == false)
	{

		mi = { sizeof(MONITORINFO) };

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullscreen = true;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

//Initialize Function
int initialize(void)
{
	//Function Declaration
	void resize(int, int);

	//Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//Code
	//Initializaton of PFD
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = { sizeof(PIXELFORMATDESCRIPTOR) };
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbientZero);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuseZero);
	glLightfv(GL_LIGHT0, GL_SPECULAR, LightSpecularZero);
	glLightfv(GL_LIGHT0, GL_POSITION, LightPositionZero);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbientOne);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuseOne);
	glLightfv(GL_LIGHT1, GL_SPECULAR, LightSpecularOne);
	glLightfv(GL_LIGHT1, GL_POSITION, LightPositionOne);
	glEnable(GL_LIGHT1);

	glLightfv(GL_LIGHT2, GL_AMBIENT, LightAmbientTwo);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, LightDiffuseTwo);
	glLightfv(GL_LIGHT2, GL_SPECULAR, LightSpecularTwo);
	glLightfv(GL_LIGHT2, GL_POSITION, LightPositionTwo);
	glEnable(GL_LIGHT2);

	glEnable(GL_LIGHTING);

	//Clear the Screen with OpenGL Color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_TEXTURE_2D);

	//Warmup call for resize
	resize(winWidth, winHeight);

	return 0;
}

//Display Function
void display(void)
{
	//Function Declaration
	mStruct* AddNode(mStruct *);
	mStruct *last = NULL;

	//Variable Declaration
	GLfloat angle = 2 * M_PI;
	GLfloat x, y;

	//Code	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.0f);
	for (int i = 0; i < 20; i++)
	{		
		
		last = AddNode(last);
		
		angle = (2 * M_PI * i) / 20;
		x = 1.5 * cos(angle);
		y = 1.5 * sin(angle);
		glPushMatrix();
		glRotatef(z_rotation, 0.0f, 0.0f, 1.0f);
		glTranslatef(x, y, 0.0f);
		glMaterialfv(GL_FRONT, GL_AMBIENT, last->MaterialAmbient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, last->MaterialDiffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, last->MaterialSpecular);
		glMaterialfv(GL_FRONT, GL_SHININESS, last->MaterialShininess);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric[i] = gluNewQuadric();
		gluSphere(quadric[i], 0.30f, 50, 50);
		glPopMatrix();

		x = 1.0 * cos(angle);
		y = 1.0 * sin(angle);
		glPushMatrix();
		glRotatef(-z_rotation, 0.0f, 0.0f, 1.0f);
		glTranslatef(x, y, 0.0f);
		glMaterialfv(GL_FRONT, GL_AMBIENT, last->MaterialAmbient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, last->MaterialDiffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, last->MaterialSpecular);
		glMaterialfv(GL_FRONT, GL_SHININESS, last->MaterialShininess);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric[i] = gluNewQuadric();
		gluSphere(quadric[i], 0.25f, 50, 50);
		glPopMatrix();

		x = 0.5 * cos(angle);
		y = 0.5 * sin(angle);
		glPushMatrix();
		glRotatef(z_rotation, 0.0f, 0.0f, 1.0f);
		glTranslatef(x, y, 0.0f);
		glMaterialfv(GL_FRONT, GL_AMBIENT, last->MaterialAmbient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, last->MaterialDiffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, last->MaterialSpecular);
		glMaterialfv(GL_FRONT, GL_SHININESS, last->MaterialShininess);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		quadric[i] = gluNewQuadric();
		gluSphere(quadric[i], 0.25f, 50, 50);
		glPopMatrix();
	}

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, translate_z);
	glMaterialfv(GL_FRONT, GL_AMBIENT, last->MaterialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, last->MaterialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, last->MaterialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, last->MaterialShininess);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric[0] = gluNewQuadric();
	gluSphere(quadric[0], 0.20f, 50, 50);
	glPopMatrix();

	SwapBuffers(ghdc);
}

//Resize Function
void resize(int winWidth, int winHeight)
{
	//Code
	if (winHeight == 0)
	{
		winHeight = 1;
	}

	glViewport(0, 0, (GLsizei)winWidth, (GLsizei)winHeight);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)winWidth / (GLfloat)winHeight, 0.1f, 100.0f);
}

//Adding Node
mStruct* AddNode(mStruct *temp)
{
	//Code
	if (temp == NULL)
	{
		mStruct *temp = (mStruct *)malloc(sizeof(mStruct));

		temp->MaterialAmbient[0] = (1.0f / rand()) * 100.f;
		temp->MaterialAmbient[1] = (1.0f / rand()) * 100.f;
		temp->MaterialAmbient[2] = (1.0f / rand()) * 100.f;
		temp->MaterialAmbient[3] = (1.0f / rand()) * 100.f;

		temp->MaterialDiffuse[0] = (1.0f / rand()) * 100.f;
		temp->MaterialDiffuse[1] = (1.0f / rand()) * 100.f;
		temp->MaterialDiffuse[2] = (1.0f / rand()) * 100.f;
		temp->MaterialDiffuse[3] = (1.0f / rand()) * 100.f;

		temp->MaterialSpecular[0] = (1.0f / rand()) * 100.f;
		temp->MaterialSpecular[1] = (1.0f / rand()) * 100.f;
		temp->MaterialSpecular[2] = (1.0f / rand()) * 100.f;
		temp->MaterialSpecular[3] = (1.0f / rand()) * 100.f;

		temp->MaterialShininess[0] = 192.0f;

		temp->next = temp;

		return temp;
	}
	
	mStruct *temp1 = (mStruct *)malloc(sizeof(mStruct));

	temp1->MaterialAmbient[0] = (1.0f / rand()) * 100.f;
	temp1->MaterialAmbient[1] = (1.0f / rand()) * 100.f;
	temp1->MaterialAmbient[2] = (1.0f / rand()) * 100.f;
	temp1->MaterialAmbient[3] = (1.0f / rand()) * 100.f;

	temp1->MaterialDiffuse[0] = (1.0f / rand()) * 100.f;
	temp1->MaterialDiffuse[1] = (1.0f / rand()) * 100.f;
	temp1->MaterialDiffuse[2] = (1.0f / rand()) * 100.f;
	temp1->MaterialDiffuse[3] = (1.0f / rand()) * 100.f;

	temp1->MaterialSpecular[0] = (1.0f / rand()) * 100.f;
	temp1->MaterialSpecular[1] = (1.0f / rand()) * 100.f;
	temp1->MaterialSpecular[2] = (1.0f / rand()) * 100.f;
	temp1->MaterialSpecular[3] = (1.0f / rand()) * 100.f;

	temp1->MaterialShininess[0] = 192.0f;

	temp1->next = temp->next;
	temp->next = temp;
	temp = temp1;

	return temp;

}

//Update
void update(void)
{
	if (translate_z >= -12.0)
	{
		translate_z = translate_z - 0.1;
	}

	if (translate_z <= -12.0)
	{
		translate_z = 10.0;
	}

	if (z_rotation <= 360.0)
	{
		z_rotation = z_rotation + 0.1;
	}

	if (z_rotation >= 360.0)
	{
		z_rotation = 0.0f;
	}
}

//Unintialize Function
void uninitialize(void)
{
	//Code
	if (gbFullscreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gbFile)
	{
		fprintf(gbFile, "Log file is Closed Successfully\n");
		fclose(gbFile);
		gbFile = NULL;
	}

	for (int i = 0; i < 20; i++)
	{
		if (quadric[i])
		{
			gluDeleteQuadric(quadric[i]);
			quadric[i] = NULL;
		}

		if (last)
		{
			free(last);
			last = NULL;
		}
	}
}
