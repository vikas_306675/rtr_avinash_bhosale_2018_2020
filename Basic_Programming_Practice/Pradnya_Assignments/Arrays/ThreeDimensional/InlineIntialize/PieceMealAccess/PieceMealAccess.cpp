#include <stdio.h>
int main(void)
{
	//Variable Declaration
	int iArray[5][3][2] = { { { 9, 18 }, { 27, 36 }, { 45, 54 } },
							{ { 8, 16 }, { 24, 32 }, { 40, 48 } },
							{ { 7, 14 }, { 21, 28 }, { 35, 42 } },
							{ { 6, 12 }, { 18, 24 }, { 30, 36 } },
							{ { 5, 10 }, { 15, 20 }, { 25, 30 } } };
	int int_size, iArray_size, iArray_num_elements, iArray_width, iArray_height, iArray_depth;

	//Code
	int_size = sizeof(int);

	iArray_size = sizeof(iArray);
	printf("\nSize of the Three Dimensional Integer Array = %d\n", iArray_size);

	iArray_width = iArray_size / sizeof(iArray[0]);
	printf("\nNumber of Rows (Width) in the Three Dimensional Integer Array = %d\n", iArray_width);

	iArray_height = sizeof(iArray[0]) / sizeof(iArray[0][0]);
	printf("\nNumber of Columns (Height) in the Three Dimensional Integer Array = %d\n", iArray_height);

	iArray_depth = sizeof(iArray[0][0]) / int_size;
	printf("\nDepth in the Three Dimensional Integer Array Is = %d\n", iArray_depth);

	iArray_num_elements = iArray_width * iArray_height * iArray_depth;
	printf("\nNumber of Elements in the Three Dimensional Integer Array = %d\n", iArray_num_elements);

	printf("\nElements in the 3D Integer Array: \n");

	printf("\n****** ROW 1 ******\n");
	printf("\n****** COLUMN 1 ******\n");
	printf("iArray[0][0][0] = %d\n", iArray[0][0][0]);
	printf("iArray[0][0][1] = %d\n", iArray[0][0][1]);

	printf("\n****** COLUMN 2 ******\n");
	printf("iArray[0][1][0] = %d\n", iArray[0][1][0]);
	printf("iArray[0][1][1] = %d\n", iArray[0][1][1]);

	printf("\n****** COLUMN 3 ******\n");
	printf("iArray[0][2][0] = %d\n", iArray[0][2][0]);
	printf("iArray[0][2][1] = %d\n", iArray[0][2][1]);

	printf("\n****** ROW 2 ******\n");
	printf("\n****** COLUMN 1 ******\n");
	printf("iArray[1][0][0] = %d\n", iArray[1][0][0]);
	printf("iArray[1][0][1] = %d\n", iArray[1][0][1]);

	printf("\n****** COLUMN 2 ******\n");
	printf("iArray[1][1][0] = %d\n", iArray[1][1][0]);
	printf("iArray[1][1][1] = %d\n", iArray[1][1][1]);

	printf("\n****** COLUMN 3 ******\n");
	printf("iArray[1][2][0] = %d\n", iArray[1][2][0]);
	printf("iArray[1][2][1] = %d\n", iArray[1][2][1]);

	printf("\n****** ROW 3 ******\n");
	printf("\n****** COLUMN 1 ******\n");
	printf("iArray[2][0][0] = %d\n", iArray[2][0][0]);
	printf("iArray[2][0][1] = %d\n", iArray[2][0][1]);

	printf("\n****** COLUMN 2 ******\n");
	printf("iArray[2][1][0] = %d\n", iArray[2][1][0]);
	printf("iArray[2][1][1] = %d\n", iArray[2][1][1]);

	printf("\n****** COLUMN 3 ******\n");
	printf("iArray[2][2][0] = %d\n", iArray[2][2][0]);
	printf("iArray[2][2][1] = %d\n", iArray[2][2][1]);

	printf("\n****** ROW 4 ******\n");
	printf("\n****** COLUMN 1 ******\n");
	printf("iArray[3][0][0] = %d\n", iArray[3][0][0]);
	printf("iArray[3][0][1] = %d\n", iArray[3][0][1]);

	printf("\n****** COLUMN 2 ******\n");
	printf("iArray[3][1][0] = %d\n", iArray[3][1][0]);
	printf("iArray[3][1][1] = %d\n", iArray[3][1][1]);

	printf("\n****** COLUMN 3 ******\n");
	printf("iArray[3][2][0] = %d\n", iArray[3][2][0]);
	printf("iArray[3][2][1] = %d\n", iArray[3][2][1]);

	printf("\n****** ROW 5 ******\n");
	printf("\n****** COLUMN 1 ******\n");
	printf("iArray[4][0][0] = %d\n", iArray[4][0][0]);
	printf("iArray[4][0][1] = %d\n", iArray[4][0][1]);

	printf("\n****** COLUMN 2 ******\n");
	printf("iArray[4][1][0] = %d\n", iArray[4][1][0]);
	printf("iArray[4][1][1] = %d\n", iArray[4][1][1]);

	printf("\n****** COLUMN 3 ******\n");
	printf("iArray[4][2][0] = %d\n", iArray[4][2][0]);
	printf("iArray[4][2][1] = %d\n", iArray[4][2][1]);

	return 0;
}

