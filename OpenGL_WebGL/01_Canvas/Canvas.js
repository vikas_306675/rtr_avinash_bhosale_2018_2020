//Onload function
function main()
{
	//Get canvas element
	var canvas_ab = document.getElementById("AMC");
	if(!canvas_ab)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");

	//print Canvas width and height on console
	console.log("Canvas Width :"+canvas_ab.width+" and Canvas Height :"+canvas_ab.height);

	//Get Ordinary 2D Context from the Canvas
	var context = canvas_ab.getContext("2d");
	if(!context)
		console.log("Obtaining 2D Context Failed\n");
	else
		console.log("Obtaining 2D Context Succeeded\n");

	//Fill Canvas with Black color
	context.fillStyle="black"; //"#000000"
	context.fillRect(0,0,canvas_ab.width,canvas_ab.height);

	//Center the Text
	context.textAlign="center"; //Center Horizontally
	context.textBaseline="middle"; //Center Vertically

	//Text
	var str="Hello World!!";

	//Text Font
	context.font="48px sans-serif";
	
	//Text Color
	context.fillStyle="white"; //"#FFFFFF"

	//Display the Text in Center
	context.fillText(str,canvas_ab.width/2,canvas_ab.height/2);
}