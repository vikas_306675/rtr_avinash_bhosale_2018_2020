#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j, k;

	//Code
	printf("\n");

	i = 0;
	do
	{
		printf("i = %d\n", i);
		printf("---------\n");
		j = 1;
		do
		{
			printf("\tj = %d\n", j);
			printf("-----------\n");
			k = 1;
			do
			{
				printf("k = %d\n", k);
				k++;
			} while (k <= 3);

			j++;
			printf("\n");

		} while (j <= 5);

		i++;
		printf("\n");

	} while (i <= 10);

	printf("All 3 loops end here\n");

	return 0;
}