#include<stdio.h>

int main(void)
{
	//Variable Declaration
	float f, fnum = 1.7f;

	//Code
	printf("\nPrinting Numbers %f to %f - ", fnum, (fnum * 10.0f));

	for (f = fnum; f <= (fnum * 10.0f); f = f + fnum)
	{
		printf("\n%f", f);
	}

	printf("\nFor Loop Completed.\n");

	return 0;
}