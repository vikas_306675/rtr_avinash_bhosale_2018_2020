#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j;

	//Code
	printf("\n");

	for (i = 0; i <= 10; i++)
	{
		printf("i = %d\n", i);
		printf("---------\n");
		for (j = 1; j <= 5; j++)
		{
			printf("\tj = %d\n", j);
		}
		printf("\n");
	}

	printf("Both loops end here\n");

	return 0;
}