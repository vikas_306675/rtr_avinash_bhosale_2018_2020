#include<Windows.h>

//Global Function
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCommandLine, int iCmdShow)
{
	//Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("WM_CHAR");

	//Code
	//Creating BluePrint for the window
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Registering the class
	RegisterClassEx(&wndclass);

	//Creating the Window
	hwnd = CreateWindow(szAppName,
		TEXT("My Window - Avinash Bhosale"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);

	//Show the Window
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//MessageLoop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

//Callback Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
			MessageBox(hwnd, TEXT("'This is Upper Case 'F' !!"), TEXT("WindowMessage"), MB_OK);
			break;
			
		case 'f':
			MessageBox(hwnd, TEXT("This is Lower Case 'f' !!"), TEXT("WindowMessage"), MB_OK);
			break;
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			MessageBox(hwnd, TEXT("ESC key is Pressed!! GoodBye!!"), TEXT("WindowMessage"), MB_OK);
			DestroyWindow(hwnd);
			break;
		}
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}