#include <stdio.h>
#include <string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declaration
	int MyStrlen(char[]);

	//Variable Declarations
	char strArray[10][15] = { "Hello!", "Welcome", "To", "Real", "Time", "Rendering", "Batch", "(2018-19-20)", "Of", "ASTROMEDICOMP." };
	int iStrLengths[10], strArray_size, strArray_num_rows, i, j;

	//Code
	strArray_size = sizeof(strArray);
	strArray_num_rows = strArray_size / sizeof(strArray[0]);

	for (i = 0; i < strArray_num_rows; i++)
	{
		iStrLengths[i] = MyStrlen(strArray[i]);
	}

	printf("\nThe Entire String Array: \n");
	for (i = 0; i < strArray_num_rows; i++)
	{
		printf("%s ", strArray[i]);
	}

	printf("\nStrings in the 2D Array: \n");

	for (i = 0; i < strArray_num_rows; i++)
	{
		printf("\nString Number %d => %s\n", (i + 1), strArray[i]);
		for (j = 0; j < iStrLengths[i]; j++)
		{
			printf("Character %d = %c\n", (j + 1), strArray[i][j]);
		}
	}

	return 0;
}

//Function for calculating string length
int MyStrlen(char str[])
{
	//Variable Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return(string_length);
}