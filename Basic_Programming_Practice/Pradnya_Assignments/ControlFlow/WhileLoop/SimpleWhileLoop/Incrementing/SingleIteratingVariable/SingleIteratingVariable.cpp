#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i;

	//Code
	printf("\nPrinting Digits 1 to 10 - ");

	i = 1;
	while( i <= 10)
	{
		printf("\n%d", i);
		i++;
	}

	printf("\nWhile Loop Completed.\n");

	return 0;
}