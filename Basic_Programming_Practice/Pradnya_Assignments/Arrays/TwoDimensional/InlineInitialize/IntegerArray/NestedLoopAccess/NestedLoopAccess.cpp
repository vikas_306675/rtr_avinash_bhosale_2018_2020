#include <stdio.h>

int main(void)
{
	//Variable Declaraions
	int iArray[5][3] = { {1, 2, 3}, {2, 4, 6}, {3, 6, 9}, {4, 8, 12}, {5, 10, 15} };
	int int_size, iArray_size, iArray_num_elements, iArray_num_rows, iArray_num_columns;
	int i, j;

	//Code
	int_size = sizeof(int);

	iArray_size = sizeof(iArray);
	printf("\nSize of Two Dimensional Integer Array = %d\n", iArray_size);

	iArray_num_rows = iArray_size / sizeof(iArray[0]);
	printf("\nNumber of Rows in Two Dimensional Integer Array = %d\n", iArray_num_rows);

	iArray_num_columns = sizeof(iArray[0]) / int_size;
	printf("\nNumber of Columns in Two Dimensional Integer Array = %d\n", iArray_num_columns);

	iArray_num_elements = iArray_num_rows * iArray_num_columns;
	printf("\nNumber of Elements in Two Dimensional Integer Array = %d\n", iArray_num_elements);

	printf("\nElements in the 2D Array: \n");

	for (i = 0; i < iArray_num_rows; i++)
	{
		printf("\n****** ROW %d ******\n", (i + 1));
		for (j = 0; j < iArray_num_columns; j++)
		{
			printf("\niArray[%d][%d] = %d\n", i, j, iArray[i][j]);
		}
	}

	return 0;
}

