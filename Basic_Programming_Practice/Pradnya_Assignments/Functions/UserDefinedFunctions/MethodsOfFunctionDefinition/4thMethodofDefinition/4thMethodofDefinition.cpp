#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	//Function declaraton
	int MyAddition(int, int);

	//Variable Declaration
	int a, b, result;

	//Code
	printf("\nEnter Integer Value for A: ");
	scanf("%d", &a);

	printf("\nEnter Integer Value for B: ");
	scanf("%d", &b);

	result = MyAddition(a, b);

	printf("\nSum of %d and %d = %d\n", a, b, result);

	return 0;
}


//User Definied function with int as return value and 2 parameters
int MyAddition(int a, int b)
{
	//Variable Declaraton
	int sum;

	//Code
	sum = a + b;

	return(sum);
}