#include<stdio.h>

int main(void)
{
	//variable declaration
	int a, b, p, q;
	char ch_result_1, ch_result_2;
	int i_result_1, i_result_2;

	//code
	a = 7;
	b = 5;

	ch_result_1 = (a > b) ? 'A' : 'B';
	i_result_1 = (a > b) ? a : b;
	printf("\nTernary Operation 1 Answer ----- %c and %d.\n", ch_result_1, i_result_1);

	p = 30;
	q = 30;
	ch_result_2 = (p != q) ? 'P' : 'Q';
	i_result_2 = (p != q) ? p : q;
	printf("\nTernary Operation 2 Answer ----- %c and %d.\n", ch_result_2, i_result_2);

	return 0;
}