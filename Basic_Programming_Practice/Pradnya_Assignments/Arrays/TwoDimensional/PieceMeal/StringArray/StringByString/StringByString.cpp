#include <stdio.h>
#include<string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declaration
	void MyStrcpy(char[], char[]);

	//Variable Declarations;
	char strArray[5][10];
	int char_size, strArray_size, strArray_num_elements, strArray_num_rows, strArray_num_columns;
	int i;

	//Code
	char_size = sizeof(char);

	strArray_size = sizeof(strArray);
	printf("\nSize of the Two Dimensional Character Array (String Array) = %d\n", strArray_size);

	strArray_num_rows = strArray_size / sizeof(strArray[0]);
	printf("\nNumber of Rows (Strings) in the Two Dimensional Character Array (String Array) = %d\n", strArray_num_rows);

	strArray_num_columns = sizeof(strArray[0]) / char_size;
	printf("\nNumber of Columns in the Two Dimensional Character Array (String Array) = %d\n", strArray_num_columns);

	strArray_num_elements = strArray_num_rows * strArray_num_columns;
	printf("\nMaximum Number of Elements (Characters) in the Two Dimensional Character Array (String Array) = %d\n", strArray_num_elements);

	MyStrcpy(strArray[0], "My");
	MyStrcpy(strArray[1], "Name");
	MyStrcpy(strArray[2], "Is");
	MyStrcpy(strArray[3], "Avinash");
	MyStrcpy(strArray[4], "Bhosale");

	printf("\nThe Strings in the 2D Character Array are: \n");

	for (i = 0; i < strArray_num_rows; i++)
	{
		printf("%s ", strArray[i]);
	}

	return 0;
}

//Function to copy strings into the character array
void MyStrcpy(char str_destination[], char str_source[])
{
	//Function Declaration
	int MyStrlen(char[]);

	//Variable Declarations
	int iStringLength = 0, j;

	//Code
	iStringLength = MyStrlen(str_source);
	for (j = 0; j < iStringLength; j++)
	{
		str_destination[j] = str_source[j];
	}

	str_destination[j] = '\0';
}

int MyStrlen(char str[])
{
	//Variable Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return(string_length);
}
