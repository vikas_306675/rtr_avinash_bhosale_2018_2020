#include <stdio.h>

int main(void)
{
	//Variable Declarations
	int iArray[5][3][2] = { { { 9, 18 }, { 27, 36 }, { 45, 54 } },
							{ { 8, 16 }, { 24, 32 }, { 40, 48 } },
							{ { 7, 14 }, { 21, 28 }, { 35, 42 } },
							{ { 6, 12 }, { 18, 24 }, { 30, 36 } },
							{ { 5, 10 }, { 15, 20 }, { 25, 30 } } };
	int int_size, iArray_size, iArray_num_elements, iArray_width, iArray_height, iArray_depth;
	int i, j, k;

	//Code
	int_size = sizeof(int);

	iArray_size = sizeof(iArray);
	printf("\nSize of the Three Dimensional Integer Array = %d\n", iArray_size);

	iArray_width = iArray_size / sizeof(iArray[0]);
	printf("\nNumber of Rows (Width) in the Three Dimensional Integer Array = %d\n", iArray_width);

	iArray_height = sizeof(iArray[0]) / sizeof(iArray[0][0]);
	printf("\nNumber of Columns (Height) in the Three Dimensional Integer Array = %d\n", iArray_height);

	iArray_depth = sizeof(iArray[0][0]) / int_size;
	printf("\nDepth in the Three Dimensional Integer Array = %d\n", iArray_depth);

	iArray_num_elements = iArray_width * iArray_height * iArray_depth;
	printf("\nNumber of Elements in the Three Dimensional Integer Array = %d\n", iArray_num_elements);

	printf("\nElements in the 3D Integer Array: \n");

	for (i = 0; i < iArray_width; i++)
	{
		printf("\n****** ROW %d ******\n", (i + 1));
		for (j = 0; j < iArray_height; j++)
		{
			printf("\n****** COLUMN %d ******\n", (j + 1));
			for (k = 0; k < iArray_depth; k++)
			{
				printf("iArray[%d][%d][%d] = %d\n", i, j, k, iArray[i][j][k]);
			}
		}
	}

	return 0;
}

