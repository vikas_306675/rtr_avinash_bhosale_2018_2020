#include<windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#define _USE_MATH_DEFINES 1
#include<math.h>

//Linking OpenGL Libraries
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Global Variable Declaration
HWND ghwnd;
WINDOWPLACEMENT wpPrev;
FILE *gbFile = NULL;
DWORD dwStyle;
HDC ghdc;
HGLRC ghrc;
bool gbDone = false;
bool gbActive = false;
bool gbFullscreen = false;
int winWidth = 800;
int winHeight = 600;

//Global Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Local Function Declaration
	int initialize(void);
	void display(void);

	//Local Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");
	int iRet;

	//Code
	//Creating Log File
	if (fopen_s(&gbFile, "Log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gbFile, "Log File has been created Successfully\n");
	}

	//Creating BluePrint for the window
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;

	//Registering the above class
	RegisterClassEx(&wndclass);

	//Creating the Window in the memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("My OpenGL Window - Avinash Bhosale"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		winWidth,
		winHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	fprintf(gbFile, "Window Created Successfully\n");

	//Storing the window handle into global handle
	ghwnd = hwnd;

	//Initialization of OpenGL
	iRet = initialize();

	//Error Checking for Initialization
	if (iRet == -1)
	{
		fprintf(gbFile, "Choosing Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gbFile, "Setting Pixel Format Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gbFile, "Create Context Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gbFile, "Make Current Context Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gbFile, "Initialization is Successful\n");
	}

	//Showing the Window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (msg.message == WM_QUIT)
			{
				gbDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == true)
			{
				//Call Update here
			}
			display();
		}
	}

	return((int)msg.wParam);
}

//Callback Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void resize(int, int);
	void uninitialize(void);
	void FullscreenToggle(void);

	//Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = true;
		break;

	case WM_KILLFOCUS:
		gbActive = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		winWidth = LOWORD(lParam);
		winHeight = HIWORD(lParam);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			FullscreenToggle();
			break;
		}
		break;

	case WM_ERASEBKGND:
		return(0);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//Fullscreen Toggle
void FullscreenToggle(void)
{
	//Variable Declaration
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullscreen = true;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

//Initialize
int initialize(void)
{
	//Function Declaration
	void resize(int, int);

	//Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//Code
	//Initializing PFD Structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = { sizeof(PIXELFORMATDESCRIPTOR) };
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	//Set the State for OpenGL BackGround Color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//WarmUp Call for resize
	resize(winWidth, winHeight);

	return 0;
}

//Display
void display(void)
{
	//Variable Declaration
	GLfloat x, y;

	// Code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);
	
	//Shape1
	for (x = -1.4; x <= -1.0; x = x + 0.10)
	{
		for (y = 1.0; y >= 0.6; y = y - 0.10)
		{
			glPointSize(2.0);
			glBegin(GL_POINTS);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(x, y, 0.0f);
			glEnd();
		}
	}

	//Shape2
	for (x = -0.3; x <= -0.1; x = x + 0.10)
	{
		for (y = 1.0; y >= 0.7; y = y - 0.10)
		{
			glBegin(GL_LINE_LOOP);
			glVertex3f(x, y, 0.0f);
			glVertex3f(x, y - 0.10, 0.0f);
			glVertex3f(x + 0.10, y, 0.0f);
			glEnd();
		}
	}

	//Shape 3
	for (x = 0.7; x <= 1.1; x = x + 0.10)
	{
		y = 1.0;
		glBegin(GL_LINES);
		glVertex3f(x, y, 0.0f);
		glVertex3f(x, y - 0.3, 0.0f);
		glEnd();
	}

	for (y = 1.0; y >= 0.6; y = y - 0.10)
	{
		x = 1.0;
		glBegin(GL_LINES);
		glVertex3f(x, y, 0.0f);
		glVertex3f(x - 0.3, y, 0.0f);
		glEnd();
	}

	//Shape 4
	for (x = -1.4; x <= -1.1; x = x + 0.10)
	{
		for (y = -0.6; y <= -0.4; y = y + 0.10)
		{
			glBegin(GL_LINE_LOOP);
			glVertex3f(x, y, 0.0f);
			glVertex3f(x, y - 0.10, 0.0f);
			glVertex3f(x + 0.10, y, 0.0f);
			glEnd();
		}
	}
	glBegin(GL_LINES);
	glVertex3f(-1.4, -0.7, 0.0f);
	glVertex3f(-1.1, -0.7, 0.0f);
	glVertex3f(-1.1, -0.7, 0.0f);
	glVertex3f(-1.1, -0.4, 0.0f);
	glEnd();

	//Shape 5
	glBegin(GL_LINES);
	glVertex3f(0.0, -0.4, 0.0f);
	glVertex3f(-0.3, -0.4, 0.0f);
	glVertex3f(-0.3, -0.4, 0.0f);
	glVertex3f(-0.3, -0.7, 0.0f);
	glVertex3f(-0.3, -0.7, 0.0f);
	glVertex3f(0.0, -0.7, 0.0f);
	glVertex3f(0.0, -0.7, 0.0f);
	glVertex3f(0.0, -0.4, 0.0f);
	glEnd();
	glBegin(GL_LINES);
	glVertex3f(-0.3, -0.4, 0.0f);
	glVertex3f(-0.2, -0.7, 0.0f);
	glVertex3f(-0.3, -0.4, 0.0f);
	glVertex3f(-0.1, -0.7, 0.0f);
	glVertex3f(-0.3, -0.4, 0.0f);
	glVertex3f(0.0, -0.7, 0.0f);
	glVertex3f(-0.3, -0.4, 0.0f);
	glVertex3f(0.0, -0.6, 0.0f);
	glVertex3f(-0.3, -0.4, 0.0f);
	glVertex3f(0.0, -0.5, 0.0f);
	glEnd();

	//Shape 6
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0, -0.4, 0.0f);
	glVertex3f(0.9, -0.4, 0.0f);
	glVertex3f(0.9, -0.5, 0.0f);
	glVertex3f(1.0, -0.5, 0.0f);
	glVertex3f(1.0, -0.5, 0.0f);
	glVertex3f(0.9, -0.5, 0.0f);
	glVertex3f(0.9, -0.6, 0.0f);
	glVertex3f(1.0, -0.6, 0.0f);
	glVertex3f(1.0, -0.6, 0.0f);
	glVertex3f(0.9, -0.6, 0.0f);
	glVertex3f(0.9, -0.7, 0.0f);
	glVertex3f(1.0, -0.7, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.9, -0.4, 0.0f);
	glVertex3f(0.8, -0.4, 0.0f);
	glVertex3f(0.8, -0.5, 0.0f);
	glVertex3f(0.9, -0.5, 0.0f);
	glVertex3f(0.9, -0.5, 0.0f);
	glVertex3f(0.8, -0.5, 0.0f);
	glVertex3f(0.8, -0.6, 0.0f);
	glVertex3f(0.9, -0.6, 0.0f);
	glVertex3f(0.9, -0.6, 0.0f);
	glVertex3f(0.8, -0.6, 0.0f);
	glVertex3f(0.8, -0.7, 0.0f);
	glVertex3f(0.9, -0.7, 0.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.8, -0.4, 0.0f);
	glVertex3f(0.7, -0.4, 0.0f);
	glVertex3f(0.7, -0.5, 0.0f);
	glVertex3f(0.8, -0.5, 0.0f);
	glVertex3f(0.8, -0.5, 0.0f);
	glVertex3f(0.7, -0.5, 0.0f);
	glVertex3f(0.7, -0.6, 0.0f);
	glVertex3f(0.8, -0.6, 0.0f);
	glVertex3f(0.8, -0.6, 0.0f);
	glVertex3f(0.7, -0.6, 0.0f);
	glVertex3f(0.7, -0.7, 0.0f);
	glVertex3f(0.8, -0.7, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0, -0.5, 0.0f);
	glVertex3f(0.7, -0.5, 0.0f);
	glVertex3f(1.0, -0.6, 0.0f);
	glVertex3f(0.7, -0.6, 0.0f);

	glVertex3f(0.8, -0.4, 0.0f);
	glVertex3f(0.8, -0.7, 0.0f);
	glVertex3f(0.9, -0.4, 0.0f);
	glVertex3f(0.9, -0.7, 0.0f);
	glEnd();

	SwapBuffers(ghdc);
}

//Resize
void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}


void uninitialize(void)
{
	if (gbFullscreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gbFile)
	{
		fprintf(gbFile, "Log File is Closed Successfully\n");
		fclose(gbFile);
		gbFile = NULL;
	}
}