#include <stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

struct MyData data = { 10, 6.2f, 9.5678, 'Q' };

int main(void)
{
	//Code
	printf("\nData Members of 'struct MyData': \n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);
	printf("c = %c\n\n", data.c);
	
	return 0;
}
