#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i;

	//Code
	printf("\nPrinting Digits 1 to 10 - ");

	for (i = 1; i <= 10; i++)
	{
		printf("\n%d", i);
	}

	printf("\nFor Loop Completed.\n");

	return 0;
}