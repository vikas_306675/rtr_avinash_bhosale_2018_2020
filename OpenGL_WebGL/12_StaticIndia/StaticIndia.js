//Global Variables
var canvas_ab=null;
var gl=null;
var bFullscreen_ab=false;
var canvas_original_width_ab;
var canvas_original_height_ab;

const WebGLMacros = 
{
AMC_ATTRIBUTE_VERTEX:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject_ab;
var fragmentShaderObject_ab;
var shaderProgramObject_ab;

var vao_il_ab, vao_n_ab, vao_d_ab, vao_ir_ab, vao_a_ab, vao_strips_ab;
var vbo_il_position_ab, vbo_il_color_ab, vbo_n_position_ab, vbo_n_color_ab, vbo_d_position_ab, vbo_d_color_ab;
var vbo_ir_position_ab, vbo_ir_color_ab, vbo_a_position_ab, vbo_a_color_ab, vbo_strips_position_ab, vbo_strips_color_ab;
var mvpuniform;
var perspectiveProjectionMatrix;

//To start Animation
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop Animation
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

//Onload function
function main()
{
	//Get Canvas element
	canvas_ab = document.getElementById("AMC");
	if(!canvas_ab)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");

	canvas_original_width_ab=canvas_ab.width;
	canvas_original_height_ab=canvas_ab.height;

	//Register event handlers
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//Initialize WebGL
	init();

	//Start Drawing
	resize();
	draw();
}

function toggleFullscreen()
{
	//Code
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	//If Fullscreen is not enabled
	if(fullscreen_element==null)
	{
		if(canvas_ab.requestFullscreen)
			canvas_ab.requestFullscreen();
		else if(canvas_ab.mozRequestFullScreen)
			canvas_ab.mozRequestFullScreen();
		else if (canvas_ab.webkitRequestFullscreen)
			canvas_ab.webkitRequestFullscreen();
		else if(canvas_ab.msRequestFullscreen)
			canvas_ab.msRequestFullscreen();
		bFullscreen_ab=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen_ab=false;
	}
}

function init()
{
    //Code
	//Get the WebGL 2.0 Context
	gl = canvas_ab.getContext("webgl2"); //webgl1 in case of mobile browser

	//If Getting Context Fails
	if(gl == null)
	{
		console.log("Failed to get the Rendering Context for WebGL");
		return;
	}
	
	gl.viewportWidth = canvas_ab.width;
	gl.viewpoetHeight = canvas_ab.height;

	//Vertex Shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
    "in vec4 vColor;" +
	"uniform mat4 u_mvp_matrix;" +
    "out vec4 out_color;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
    "out_color = vColor;" +
	"}";
	
	vertexShaderObject_ab = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_ab, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject_ab);
	
	if(gl.getShaderParameter(vertexShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.ShaderInfoLog(vertexShaderObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Fragment Shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
    "in vec4 out_color;" +
	"out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "FragColor = out_color;" +
    "}";
	
	fragmentShaderObject_ab = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_ab, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject_ab);
	
	if(gl.getShaderParameter(fragmentShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.ShaderInfoLog(fragmentShaderObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Shader Program
	shaderProgramObject_ab = gl.createProgram();
	gl.attachShader(shaderProgramObject_ab, vertexShaderObject_ab);
	gl.attachShader(shaderProgramObject_ab, fragmentShaderObject_ab);

	//Pre-Linking of the Shader Program Object with Vertex Shader Attributes
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");
	
	//Linking
	gl.linkProgram(shaderProgramObject_ab);
	
	if(gl.getProgramParameter(shaderProgramObject_ab, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Get MVP Uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject_ab, "u_mvp_matrix");

	var ilVertices = new Float32Array([-0.60, 0.60, 0.0, -0.50, 0.60, 0.0,
                                       -0.55, 0.60, 0.0, -0.55, -0.60, 0.0, 
                                       -0.60, -0.60, 0.0, -0.50, -0.60, 0.0]);

	var ilColor = new Float32Array([1.00, 0.60, 0.20, 1.00, 0.60, 0.20, 
                                    1.00, 0.60, 0.20, 0.07, 0.53, 0.027,
                                    0.07, 0.53, 0.027, 0.07, 0.53, 0.027]);

	var nVertices = new Float32Array([-0.40, 0.60, 0.00, -0.40, -0.60, 0.0, 
                                      -0.40, 0.60, 0.00, -0.20, -0.60, 0.0, 
                                      -0.20, -0.60, 0.0, -0.20, 0.60, 0.0]);

	var nColor = new Float32Array([1.00, 0.60, 0.20, 0.07, 0.53, 0.027, 
                                   1.00, 0.60, 0.20, 0.07, 0.53, 0.027, 
                                   0.07, 0.53, 0.027, 1.00, 0.60, 0.20]);
    
	var dVertices = new Float32Array([-0.10, 0.60, 0.00, -0.10, -0.60, 0.0, 
                                      -0.15, 0.60, 0.00, 0.10, 0.60, 0.00, 
                                      0.10, 0.60, 0.00, 0.10, -0.60, 0.0, 
                                      0.10, -0.60, 0.0, -0.15, -0.60, 0.00]);
    
	var dColor = new Float32Array([1.00, 0.60, 0.20, 0.07, 0.53, 0.027, 
                                   1.00, 0.60, 0.20, 1.00, 0.60, 0.20, 
                                   1.00, 0.60, 0.20, 0.07, 0.53, 0.027, 
                                   0.07, 0.53, 0.027, 0.07, 0.53, 0.027]);
    
	var irVertices = new Float32Array([0.20, 0.60, 0.0, 0.30, 0.60, 0.0, 
                                       0.25, 0.60, 0.0, 0.25, -0.60, 0.0, 
                                       0.20, -0.60, 0.0, 0.30, -0.60, 0.0]);
    
	var irColor = new Float32Array([1.00, 0.60, 0.20, 1.00, 0.60, 0.20, 
                                   1.00, 0.60, 0.20, 0.07, 0.53, 0.027, 
                                   0.07, 0.53, 0.027, 0.07, 0.53, 0.027]);
    
	var aVertices = new Float32Array([0.60, -0.60, 0.0, 0.50, 0.60, 0.0, 
                                      0.50, 0.60, 0.0, 0.40, -0.60, 0.0]);

	var aColor = new Float32Array([0.07, 0.53, 0.027, 1.00, 0.60, 0.20, 
                                   1.00, 0.60, 0.20, 0.07, 0.53, 0.027]);

	var stripsVertices = new Float32Array([-0.048, 0.02, 0.0, 0.048, 0.02, 0.0, 
                                           -0.05, 0.0, 0.0, 0.05, 0.0, 0.0, 
                                           -0.052, -0.02, 0.0, 0.052, -0.02, 0.0]);

	var stripsColor = new Float32Array([1.00, 0.60, 0.20, 1.00, 0.60, 0.20, 
                                        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 
                                        0.07, 0.53, 0.027, 0.07, 0.53, 0.027]);

	//Vao for Triangle
	vao_il_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_il_ab);

	vbo_il_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_il_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, ilVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_il_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_il_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, ilColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_n_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_n_ab);

	vbo_n_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_n_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, nVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_n_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_n_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, nColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_d_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_d_ab);

	vbo_d_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_d_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, dVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_d_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_d_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, dColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_ir_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_ir_ab);

	vbo_ir_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_ir_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, irVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_ir_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_ir_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, irColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_a_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_a_ab);

	vbo_a_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_a_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, aVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_a_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_a_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, aColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_strips_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_strips_ab);

	vbo_strips_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_strips_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, stripsVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_strips_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_strips_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, stripsColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	//Set the Clear Color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	//Initialize Projection Matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//Code
	if(bFullscreen_ab == true)
	{
		canvas_ab.width=window.innerWidth;
		canvas_ab.height=window.innerHeight;
	}
	else
	{
		canvas_ab.width=canvas_original_width_ab;
		canvas_ab.height=canvas_original_height_ab;
	}

	//Set the viewport
	gl.viewport(0, 0, canvas_ab.width, canvas_ab.height);

	//Perspective Projection
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas_ab.width)/parseFloat(canvas_ab.height), 0.1, 100.0);
	
}

function draw()
{
	//Code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject_ab);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);;
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    //Draw Left I
	gl.bindVertexArray(vao_il_ab);

	gl.drawArrays(gl.LINES, 0, 6);

	gl.bindVertexArray(null);

    //Draw N
	gl.bindVertexArray(vao_n_ab);

	gl.drawArrays(gl.LINES, 0, 6);

	gl.bindVertexArray(null);

    //Draw D
	gl.bindVertexArray(vao_d_ab);

	gl.drawArrays(gl.LINES, 0, 8);

	gl.bindVertexArray(null);

    //Draw Right I
	gl.bindVertexArray(vao_ir_ab);

	gl.drawArrays(gl.LINES, 0, 6);

	gl.bindVertexArray(null);

    //Draw A
	gl.bindVertexArray(vao_a_ab);

	gl.drawArrays(gl.LINES, 0, 4);

	gl.bindVertexArray(null);

    //Translate for Drawing Strips
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.5, 0.0, -3.0]);;
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    //Draw the Strips
	gl.bindVertexArray(vao_strips_ab);

	gl.drawArrays(gl.LINES, 0, 6);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	update();

	//Animation Loop
	requestAnimationFrame(draw, canvas_ab);
}

function update()
{
    //Code
}

function uninitialize()
{
	//Code
	if (vbo_il_position_ab)
	{
	    gl.deleteBuffers(vbo_il_position_ab);
	    vbo_il_position_ab = null;
	}

	if (vbo_il_color_ab)
	{
	    gl.deleteBuffers(vbo_il_color_ab);
	    vbo_il_color_ab = null;
	}

	if (vbo_n_position_ab)
	{
	    gl.deleteBuffers(vbo_n_position_ab);
	    vbo_n_position_ab = null;
	}

	if (vbo_n_color_ab)
	{
	    gl.deleteBuffers(vbo_n_color_ab);
	    vbo_n_color_ab = null;
	}

	if (vbo_d_position_ab)
	{
	    gl.deleteBuffers(vbo_d_position_ab);
	    vbo_d_position_ab = null;
	}

	if (vbo_d_color_ab)
	{
	    gl.deleteBuffers(vbo_d_color_ab);
	    vbo_d_color_ab = null;
	}

	if (vbo_a_position_ab)
	{
	    gl.deleteBuffers(vbo_a_position_ab);
	    vbo_a_position_ab = null;
	}

	if (vbo_a_color_ab)
	{
	    gl.deleteBuffers(vbo_a_color_ab);
	    vbo_a_color_ab = null;
	}

	if (vbo_strips_position_ab)
	{
	    gl.deleteBuffers(vbo_strips_position_ab);
	    vbo_strips_position_ab = null;
	}

	if (vbo_strips_color_ab)
	{
	    gl.deleteBuffers(vbo_strips_color_ab);
	    vbo_strips_color_ab = null;
	}

	if (vbo_ir_position_ab)
	{
	    glDeleteBuffers(vbo_ir_position_ab);
	    vbo_ir_position_ab = 0;
	}

	if (vbo_ir_color_ab)
	{
	    glDeleteBuffers(vbo_ir_color_ab);
	    vbo_ir_color_ab = 0;
	}

	if (vao_il_ab)
	{
	    gl.deleteVertexArrays(vao_il_ab);
	    vao_il_ab = null;
	}

	if (vao_n_ab)
	{
	    gl.deleteVertexArrays(vao_n_ab);
	    vao_n_ab = null;
	}

	if (vao_d_ab)
	{
	    gl.deleteVertexArrays(vao_d_ab);
	    vao_d_ab = null;
	}

	if (vao_ir_ab)
	{
	    gl.deleteVertexArrays(vao_ir_ab);
	    vao_ir_ab = null;
	}

	if (vao_a_ab)
	{
	    gl.deleteVertexArrays(vao_a_ab);
	    vao_a_ab = null;
	}

	if (vao_strips_ab)
	{
	    gl.deleteVertexArrays(vao_strips_ab);
	    vao_strips_ab = null;
	}

	if(shaderProgramObject_ab)
	{
		if(fragmentShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, fragmentShaderObject_ab);
			gl.deleteShader(fragmentShaderObject_ab);
			fragmentShaderObject_ab = null;
		}

		if(vertexShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, vertexShaderObject_ab);
			gl.deleteShader(vertexShaderObject_ab);
			vertexShaderObject_ab = null;
		}

		gl.deleteProgram(shaderProgramObject_ab);
		shaderProgramObject_ab = null;
	}
}

function keyDown(event)
{
	//Code
	switch(event.keyCode)
	{

		case 27:
			//Escape for uninitialize
			uninitialize();
			//Close our Application Tab
			window.close();
			break;

		case 70: // For 'F' or 'f'
			toggleFullscreen();
			break;
	}
}

function mouseDown()
{
	//Code
}