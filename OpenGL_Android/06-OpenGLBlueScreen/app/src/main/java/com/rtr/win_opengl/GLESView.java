package com.rtr.win_opengl;

//Added packages by me
import android.content.Context; //For context
import android.graphics.Color; //For Color
import android.view.Gravity; //For Gravity
import android.view.MotionEvent; // for MotionEvent
import android.view.GestureDetector; // for GestureDetector
import android.view.GestureDetector.OnGestureListener; // for OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // OnDoubleTapListener
import android.opengl.GLSurfaceView;
import android.opengl.GLES30;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private GestureDetector gestureDetector;
    private final Context context;

    public GLESView(Context drawingContext)
    {
	super(drawingContext);
	context = drawingContext;

	setEGLContextClientVersion(2);
	setRenderer(this);
	setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	gestureDetector = new GestureDetector(context, this, null, false);
	gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
	{
            super.onTouchEvent(event);
	}
        return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do not write any code here because it is already written in 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do not write any code here because it is already written in 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }

    //Implement GLSurfaceView.Renderer Methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
	String version = gl.glGetString(GL10.GL_VERSION);
	System.out.println("RTR: "+version);
	initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
	resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
	display();
    }

    //Creating our custom methods
    private void initialize()
    {
	GLES30.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    }

    private void resize(int width, int height)
    {
	GLES30.glViewport(0, 0, width, height);
    }

    private void display()
    {
	GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
	requestRender();
    }
}
    