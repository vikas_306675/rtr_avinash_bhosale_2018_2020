#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int a, b, p;

	//Code
	a = 9;
	b = 30;
	p = 30;

	if (a < b)
	{
		printf("\nEntering 1st if Block...\n");
		printf("\nA is less than B.\n");
	}
	else
	{
		printf("\nEntering 1st else Block...\n");
		printf("\nB is less than A.\n");
	}
	printf("\nFirst if-else Block is done!\n");

	if (b != p)
	{
		printf("\nEntering 2nd if Block...\n");
		printf("\nB is not equal to P.\n");
	}
	else
	{
		printf("\nEntering 2nd else Block...\n");
		printf("\nB is equal to P.\n");
	}
	printf("\nSecond if-else Block is done!\n");

	return 0;
}