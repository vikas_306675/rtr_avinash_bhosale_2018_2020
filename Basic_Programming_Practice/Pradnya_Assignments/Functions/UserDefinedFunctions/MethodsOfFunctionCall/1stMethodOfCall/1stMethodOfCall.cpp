#include <stdio.h> 

int main(int argc, char *argv[], char *envp[])
{
	//Function declarations
	void MyAddition(void);
	int MySubtraction(void);
	void MyMultiplication(int, int);
	int MyDivision(int, int);

	//Variable declarations
	int result_subtraction;
	int a_multiplication, b_multiplication;
	int a_division, b_division, result_division;

	//Code

	//Addition
	MyAddition();

	//Subtraction
	result_subtraction = MySubtraction();

	//Multiplication
	printf("\nSubtraction gives Result = %d\n", result_subtraction);

	printf("\nEnter Integer Value for 'A' for Multiplication: ");
	scanf("%d", &a_multiplication);

	printf("\nEnter Integer Value for 'B' for Multiplication: ");
	scanf("%d", &b_multiplication);

	MyMultiplication(a_multiplication, b_multiplication);

	//Division
	printf("\nEnter Integer Value for 'A' for Division: ");
	scanf("%d", &a_division);

	printf("\nEnter Integer Value for 'B' for Division: ");
	scanf("%d", &b_division);

	result_division = MyDivision(a_division, b_division);

	printf("\nDivision Of %d and %d Gives = %d (Quotient)\n", a_division, b_division, result_division);

	return(0);
}

//Addition Function
void MyAddition(void)
{
	//Variable declarations
	int a, b, sum;

	//Code
	printf("\nEnter Integer Value for 'A' for Addition: ");
	scanf("%d", &a);

	printf("\nEnter Integer Value for 'B' for Addition: ");
	scanf("%d", &b);

	sum = a + b;

	printf("\nSum Of %d And %d = %d\n", a, b, sum);
}

//Subtraction Function
int MySubtraction(void)
{
	//Variable declarations
	int a, b, subtraction;

	//Code
	printf("\nEnter Integer Value for 'A' for Subtraction: ");
	scanf("%d", &a);

	printf("\n\n");
	printf("Enter Integer Value for 'B' for Subtraction: ");
	scanf("%d", &b);

	subtraction = a - b;
	return(subtraction);
}

//Multiplicaiton Function
void MyMultiplication(int a, int b)
{
	//Variable declarations
	int multiplication;

	//Code
	multiplication = a * b;

	printf("\nMultiplication Of %d And %d = %d\n", a, b, multiplication);
}

//Division Function
int MyDivision(int a, int b)
{
	//Variable declarations
	int division_quotient;

	//Code
	if (a > b)
	{
		division_quotient = a / b;
	}
	else
	{
		division_quotient = b / a;
	}

	return(division_quotient);
}