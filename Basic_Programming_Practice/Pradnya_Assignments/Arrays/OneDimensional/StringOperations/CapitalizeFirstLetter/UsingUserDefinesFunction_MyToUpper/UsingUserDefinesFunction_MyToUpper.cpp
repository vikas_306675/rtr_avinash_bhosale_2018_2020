#include <stdio.h>
#include<string.h>

//Macros
#define MAX_STRING_LENGTH 512
#define SPACE ' '
#define FULLSTOP '.'
#define COMMA ','
#define EXCLAMATION '!'
#define QUESTION_MARK '?'

int main(void)
{
	//Function Declarations
	int MyStrlen(char[]);
	char MyToUpper(char);

	//Variable Declarations
	char chArray[MAX_STRING_LENGTH], chArray_CapitalizedFirstLetterOfEveryWord[MAX_STRING_LENGTH]; // A Character Array Is A String
	int iStringLength, i, j;

	//Code
	printf("\nEnter a String: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);
	j = 0;
	for (i = 0; i < iStringLength; i++)
	{
		if (i == 0)
		{
			chArray_CapitalizedFirstLetterOfEveryWord[j] = MyToUpper(chArray[i]);
		}
		else if (chArray[i] == SPACE)
		{
			chArray_CapitalizedFirstLetterOfEveryWord[j] = chArray[i];
			chArray_CapitalizedFirstLetterOfEveryWord[j + 1] = MyToUpper(chArray[i + 1]);

			j++;
			i++;
		}
		else if ((chArray[i] == FULLSTOP || chArray[i] == COMMA || chArray[i] == EXCLAMATION || chArray[i] == QUESTION_MARK) && (chArray[i] != SPACE))
		{
			chArray_CapitalizedFirstLetterOfEveryWord[j] = chArray[i];
			chArray_CapitalizedFirstLetterOfEveryWord[j + 1] = SPACE;
			chArray_CapitalizedFirstLetterOfEveryWord[j + 2] = MyToUpper(chArray[i + 1]);

			j = j + 2;
			i++;
		}
		else
		{
			chArray_CapitalizedFirstLetterOfEveryWord[j] = chArray[i];
		}

		j++;
	}

	chArray_CapitalizedFirstLetterOfEveryWord[j] = '\0';

	printf("\nString Entered: \n");
	printf("%s\n", chArray);

	printf("\nString after Capitalizing the First Letter of Every Word: \n");
	printf("%s\n", chArray_CapitalizedFirstLetterOfEveryWord);

	return 0;
}

int MyStrlen(char str[])
{
	//Variable Declarations
	int j;
	int string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return(string_length);
}

char MyToUpper(char ch)
{
	//Variable Declarations
	int num, c;

	//Code
	num = 'a' - 'A';

	if ((int)ch >= 97 && (int)ch <= 122)
	{
		c = (int)ch - num;

		return((char)c);
	}
	else
	{
		return(ch);
	}
}
