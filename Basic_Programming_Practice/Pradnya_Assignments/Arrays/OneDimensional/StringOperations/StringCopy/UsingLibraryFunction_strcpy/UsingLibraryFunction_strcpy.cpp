#include <stdio.h>
#include <string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Variable Declarations 
	char chArray_Original[MAX_STRING_LENGTH], chArray_Copy[MAX_STRING_LENGTH];

	//code
	printf("\nEnter a String: \n");
	gets_s(chArray_Original, MAX_STRING_LENGTH);

	strcpy(chArray_Copy, chArray_Original);
	
	printf("\nOriginal String Entered ('chArray_Original[]'): \n");
	printf("%s\n", chArray_Original);

	printf("\nCopied String ('chArray_Copy[]'): \n");
	printf("%s\n", chArray_Copy);

	return 0;
}
