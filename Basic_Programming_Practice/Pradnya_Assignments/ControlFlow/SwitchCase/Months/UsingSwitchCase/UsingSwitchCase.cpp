#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int month;
	
	//Code
	printf("\nEnter a Month number (1 - 12): ");
	scanf("%d", &month);

	//Using switch to find the month for the number entered by the user
	switch (month)
	{
	case 1:
		printf("\nMonth Number %d is JANUARY.\n", month);
		break;

	case 2:
		printf("\nMonth Number %d is FEBRUARY.\n", month);
		break;

	case 3:
		printf("\nMonth Number %d is MARCH.\n", month);
		break;

	case 4:
		printf("\nMonth Number %d is APRIL.\n", month);
		break;

	case 5:
		printf("\nMonth Number %d is MAY.\n", month);
		break;

	case 6:
		printf("\nMonth Number %d is JUNE.\n", month);
		break;

	case 7:
		printf("\nMonth Number %d is JULY.\n", month);
		break;

	case 8:
		printf("\nMonth Number %d is AUGUST.\n", month);
		break;

	case 9:
		printf("\nMonth Number %d is SEPTEMBER.\n", month);
		break;

	case 10:
		printf("\nMonth Number %d is OCTOBER.\n", month);
		break;

	case 11:
		printf("\nMonth Number %d is NOVEMBER.\n", month);
		break;

	case 12:
		printf("\nMonth Number %d is DECEMBER.\n", month);
		break;

	default:
		printf("\nInvalid Number %d Entered!!\n", month);
		break;
	}
	
	printf("\nSwitch Block is Complete.\n");

	return 0;
}