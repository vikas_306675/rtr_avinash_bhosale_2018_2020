#include <stdio.h>
#include<string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declarations
	int MyStrlen(char[]);
	void MyStrcpy(char[], char[]);

	//Variable Declarations
	char chArray[MAX_STRING_LENGTH], chArray_SpacesRemoved[MAX_STRING_LENGTH];
	int iStringLength, i, j;

	//Code
	printf("\nEnter a String: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);
	j = 0;
	for (i = 0; i < iStringLength; i++)
	{
		if (chArray[i] == ' ')
		{
			continue;
		}
		else
		{
			chArray_SpacesRemoved[j] = chArray[i];
			j++;
		}
	}

	chArray_SpacesRemoved[j] = '\0';

	printf("\nString Entered: \n");
	printf("%s\n", chArray);

	printf("\nString after Removal of Spaces: \n");
	printf("%s\n", chArray_SpacesRemoved);

	return 0;
}

int MyStrlen(char str[])
{
	//Variable Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}

	return(string_length);
}
