#include<stdio.h>

//Entry Point function with int as return value, 2 parameters
int main(int argc, char *argv[])
{
	//Variable declaration
	int i;
	
	//Code
	printf("\nHello World!!\n");
	printf("\nNumber of Command Line Arguments: %d\n", argc);
	printf("\nCommand Line Arguments passed to this Program are: \n");

	for (i = 0; i < argc; i++)
	{
		printf("Command Line Argument Number %d = %s\n", (i+1) , argv[i]);
	}

	return 0;
}