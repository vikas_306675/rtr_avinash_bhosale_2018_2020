#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int a, b, p;

	//Code
	a = 9;
	b = 30;
	p = 30;

	if (a < b)
	{
		printf("\nA is less the B.\n");
	}

	if (b != p)
	{
		printf("\nB is not equal to P.\n");
	}

	printf("\nBoth comparisions are done!\n");

	return 0;
}