#include <stdio.h>
#include <string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Variable Declarations
	char strArray[5][10];
	int char_size, strArray_size, strArray_num_elements, strArray_num_rows, strArray_num_columns;
	int i;

	//Code
	char_size = sizeof(char);

	strArray_size = sizeof(strArray);
	printf("\nSize of the Two Dimensional Character Array (String Array) = %d\n", strArray_size);

	strArray_num_rows = strArray_size / sizeof(strArray[0]);
	printf("\nNumber of Rows (Strings) in the Two Dimensional Character Array (String Array) = %d\n", strArray_num_rows);

	strArray_num_columns = sizeof(strArray[0]) / char_size;
	printf("\nNumber of Columns in the Two Dimensional Character Array (String Array) = %d\n", strArray_num_columns);

	strArray_num_elements = strArray_num_rows * strArray_num_columns;
	printf("\nMaximum number of Elements (Characters) in the Two Dimensional Character Array (String Array) = %d\n", strArray_num_elements);

	//Row 1
	strArray[0][0] = 'M';
	strArray[0][1] = 'y';
	strArray[0][2] = '\0'; //Terminating the string with NULL character

	//Row 2
	strArray[1][0] = 'N';
	strArray[1][1] = 'a';
	strArray[1][2] = 'm';
	strArray[1][3] = 'e';
	strArray[1][4] = '\0';

	//Row 3
	strArray[2][0] = 'I';
	strArray[2][1] = 's';
	strArray[2][2] = '\0';

	//Row 4
	strArray[3][0] = 'A';
	strArray[3][1] = 'v';
	strArray[3][2] = 'i';
	strArray[3][3] = 'n';
	strArray[3][4] = 'a';
	strArray[3][5] = 's';
	strArray[3][6] = 'h';
	strArray[3][7] = '\0';

	//Row 5
	strArray[4][0] = 'B';
	strArray[4][1] = 'h';
	strArray[4][2] = 'o';
	strArray[4][3] = 's';
	strArray[4][4] = 'a';
	strArray[4][5] = 'l';
	strArray[4][6] = 'e';
	strArray[4][7] = '\0';

	printf("\nStrings in the 2D Character Array are: \n\n");

	for (i = 0; i < strArray_num_rows; i++)
	{
		printf("%s ", strArray[i]);
	}

	return 0;
}

int MyStrlen(char str[])
{
	//Variable Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return(string_length);
}