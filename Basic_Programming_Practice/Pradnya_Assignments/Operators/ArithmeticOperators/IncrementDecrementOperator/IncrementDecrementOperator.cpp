#include<stdio.h>

int main(void)
{
	//variable declaration
	int a = 5;
	int b = 10;

	//code
	printf("\nA = %d", a);
	printf("\nA = %d", a++);
	printf("\nA = %d", a);
	printf("\nA = %d", ++a);

	printf("\n");

	printf("\nB = %d", b);
	printf("\nB = %d", b--);
	printf("\nB = %d", b);
	printf("\nB = %d", --b);

	return 0;
}