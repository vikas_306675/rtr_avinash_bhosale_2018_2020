#include <stdio.h>
#include <string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Variable Declarations 
	char chArray_Original[MAX_STRING_LENGTH];

	//Code
	printf("\nEnter a String: \n");
	gets_s(chArray_Original, MAX_STRING_LENGTH);

	printf("\nThe Original String Entered ('chArray_Original[]'): \n");
	printf("%s\n", chArray_Original);

	printf("\nReversed String ('chArray_Reverse[]'): \n");
	printf("%s\n", strrev(chArray_Original));

	return 0;
}
