//Global Variables
var canvas_ab=null;
var gl=null;
var bFullscreen_ab=false;
var canvas_original_width_ab;
var canvas_original_height_ab;

const WebGLMacros = 
{
AMC_ATTRIBUTE_VERTEX:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject_ab;
var fragmentShaderObject_ab;
var shaderProgramObject_ab;

var vao_cube_ab;
var vbo_position_cube_ab, vbo_normal_cube_ab;
var modelViewuniform, projectionMatrixUniform;

var perspectiveProjectionMatrix;
var ldUniform_ab, kdUniform_ab, lightPositionUniform_ab;
var LKeyPressedUniform_ab = false;
var angle_cube = 0.0;
var keypress = 0;
var lKeyPressed = false;


//To start Animation
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop Animation
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

//Onload function
function main()
{
	//Get canvas element
	canvas_ab = document.getElementById("AMC");
	if(!canvas_ab)
		console.log("Obtaining canvas_ab Failed\n");
	else
		console.log("Obtaining canvas_ab Succeeded\n");

	canvas_original_width_ab=canvas_ab.width;
	canvas_original_height_ab=canvas_ab.height;

	//Register event handlers
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//Initialize WebGL
	init();

	//Start Drawing
	resize();
	draw();
}

function toggleFullscreen()
{
	//Code
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	//If Fullscreen is not enabled
	if(fullscreen_element==null)
	{
		if(canvas_ab.requestFullscreen)
			canvas_ab.requestFullscreen();
		else if(canvas_ab.mozRequestFullScreen)
			canvas_ab.mozRequestFullScreen();
		else if (canvas_ab.webkitRequestFullscreen)
			canvas_ab.webkitRequestFullscreen();
		else if(canvas_ab.msRequestFullscreen)
			canvas_ab.msRequestFullscreen();
		bFullscreen_ab=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen_ab=false;
	}
}

function init()
{
	//Code
	//Get the WebGL 2.0 Context
	gl = canvas_ab.getContext("webgl2"); //webgl1 in case of mobile browser

	//If Getting Context Fails
	if(gl == null)
	{
		console.log("Failed to get the Rendering Context for WebGL");
		return;
	}
	
	gl.viewportWidth = canvas_ab.width;
	gl.viewportHeight = canvas_ab.height;

	//Vertex Shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_mv_matrix;" +
    "uniform mat4 u_projection;" +
    "uniform mediump int u_lIsPressed;" +
    "uniform vec3 u_ld;" +
    "uniform vec3 u_kd;" +
    "uniform vec4 u_light_position;" +
    "out vec3 diffuse_color;" +
    "void main(void)" +
    "{" +
    "if(u_lIsPressed==1)" +
    "{" +
    "vec4 eye_coordinate = u_mv_matrix * vPosition;" +
    "mat3 normal_matrix = mat3(u_mv_matrix);" +
    "vec3 vNorm = normalize(normal_matrix * vNormal);" +
    "vec3 s = normalize(vec3(u_light_position) - eye_coordinate.xyz);" +
    "diffuse_color = u_ld * u_kd * max(dot(s, vNorm), 0.0);" +
    "}" +
    "gl_Position = u_projection * u_mv_matrix * vPosition;" +
    "}";
	
	vertexShaderObject_ab = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_ab, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject_ab);
	
	if(gl.getShaderParameter(vertexShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_ab);
		if(error.length > 0)
		{
		    console.log("Vertex Shader Error:\n");
			alert(error);
			uninitialize();
		}
	}

	//Fragment Shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"in vec3 diffuse_color;" +
    "uniform int u_lIsPressed;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
    "vec4 color;" +
    "if(u_lIsPressed == 1)" +
    "{" +
    "color = vec4(diffuse_color, 1.0);" +
    "}" +
    "else" +
    "{" +
    "color = vec4(1.0, 1.0, 1.0, 1.0);" +
    "}" +
	"FragColor = color;" +
	"}";
	
	fragmentShaderObject_ab = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_ab, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject_ab);
	
	if(gl.getShaderParameter(fragmentShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_ab);
		if(error.length > 0)
		{
		    console.log("Fragment Shader Error:\n");
			alert(error);
			uninitialize();
		}
	}

	//Shader Program
	shaderProgramObject_ab = gl.createProgram();
	gl.attachShader(shaderProgramObject_ab, vertexShaderObject_ab);
	gl.attachShader(shaderProgramObject_ab, fragmentShaderObject_ab);

	//Pre-Linking of the Shader Program Object with Vertex Shader Attributes
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");
	
	//Linking
	gl.linkProgram(shaderProgramObject_ab);
	
	if(gl.getProgramParameter(shaderProgramObject_ab, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Get MVP Uniform location
	modelViewuniform = gl.getUniformLocation(shaderProgramObject_ab, "u_mv_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject_ab, "u_projection");
	LKeyPressedUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_lIsPressed");
	ldUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_ld");
	kdUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_kd");
	lightPositionUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_light_position");
	

	var cubeVertices_ab = new Float32Array([0.45, 0.45, -0.45, 0.45, 0.45, 0.45, -0.45, 0.45, 0.45, -0.45, 0.45, -0.45,
                                        0.45, -0.45, -0.45, 0.45, -0.45, 0.45, -0.45, -0.45, 0.45, -0.45, -0.45, -0.45,
                                        0.45, 0.45, 0.45, -0.45, 0.45, 0.45, -0.45, -0.45, 0.45, 0.45, -0.45, 0.45,
                                        0.45, 0.45, -0.45, -0.45, 0.45, -0.45, -0.45, -0.45, -0.45, 0.45, -0.45, -0.45,
                                        0.45, 0.45, -0.45, 0.45, 0.45, 0.45, 0.45, -0.45, 0.45, 0.45, -0.45, -0.45,
                                        -0.45, 0.45, -0.45, -0.45, 0.45, 0.45, -0.45, -0.45, 0.45, -0.45, -0.45, -0.45]);

	var cubeNormals_ab = new Float32Array([1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 
									1.0, -1.0, 1.0, -1.0, -1.0, 1.0, -1.0, -1.0, -1.0, 1.0, -1.0, -1.0, 
									0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 
									1.0, -1.0, -1.0,  -1.0, -1.0, -1.0, -1.0, 1.0, -1.0, 1.0, 1.0, -1.0, 
									1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 
									-1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0, -1.0, 0.0, 0.0]);

	//Vao for Cube
	vao_cube_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_cube_ab);

	vbo_position_cube_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_cube_ab);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices_ab, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_normal_cube_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_normal_cube_ab);
	gl.bufferData(gl.ARRAY_BUFFER, cubeNormals_ab, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	//Set the Clear Color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	//Initialize Projection Matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//Code
	if(bFullscreen_ab == true)
	{
		canvas_ab.width=window.innerWidth;
		canvas_ab.height=window.innerHeight;
	}
	else
	{
		canvas_ab.width=canvas_original_width_ab;
		canvas_ab.height=canvas_original_height_ab;
	}

	//Set the viewport
	gl.viewport(0, 0, canvas_ab.width, canvas_ab.height);

	//Perspective Projection
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas_ab.width)/parseFloat(canvas_ab.height), 0.1, 100.0);
	
}

function draw()
{
	//Code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject_ab);

	if (lKeyPressed == true)
	{
	    gl.uniform1i(LKeyPressedUniform_ab, 1);

	    //Setting Light Properties
	    gl.uniform3f(ldUniform_ab, 1.0, 1.0, 1.0);

	    //Setting Material Properties
	    gl.uniform3f(kdUniform_ab, 0.5, 0.5, 0.5);

	    //Light Position
	    var lightPosition_ab = [0.0, 0.0, 2.0, 1.0];
	    gl.uniform4fv(lightPositionUniform_ab, lightPosition_ab);
	}
	else
    {
	    gl.uniform1i(LKeyPressedUniform_ab, 0);
	}

	var modelViewMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);
	mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));

	gl.uniformMatrix4fv(modelViewuniform, false, modelViewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

    //Bind with Texture

    //Bind with Vao for Cube
	gl.bindVertexArray(vao_cube_ab);

    //Draw the Cube
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	
    //Unbind with Vao for Cube
	gl.bindVertexArray(null);

	gl.useProgram(null);

	update();

	//Animation Loop
	requestAnimationFrame(draw, canvas_ab);
}

function update()
{
    //Code
	angle_cube = angle_cube + 1.0;

	if (angle_cube >= 360.0)
	{
		angle_cube = 0.0;
	}
}

function uninitialize()
{
	//Code
	if(vao_cube_ab)
	{
	    gl.deleteVertexArray(vao_cube_ab);
	    vao_cube_ab = null;
	}

	if (vbo_position_cube_ab)
	{
	    gl.deleteBuffer(vbo_position_cube_ab);
	    vbo_position_cube_ab = null;
	}

	if (vbo_normal_cube_ab)
	{
	    gl.deleteTexture(vbo_normal_cube_ab);
	    vbo_normal_cube_ab = 0;
	}

	if(shaderProgramObject_ab)
	{
		if(fragmentShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, fragmentShaderObject_ab);
			gl.deleteShader(fragmentShaderObject_ab);
			fragmentShaderObject_ab = null;
		}

		if(vertexShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, vertexShaderObject_ab);
			gl.deleteShader(vertexShaderObject_ab);
			vertexShaderObject_ab = null;
		}

		gl.deleteProgram(shaderProgramObject_ab);
		shaderProgramObject_ab = null;
	}
}

function keyDown(event)
{
	//Code
	switch(event.keyCode)
	{

		case 27:
			//Escape for uninitialize
			uninitialize();
			//Close our Application Tab
			window.close();
			break;

		case 70: // For 'F' or 'f'
			toggleFullscreen();
			break;

	    case 76:
	        if(lKeyPressed == false)
	        {
	            lKeyPressed = true;
	        }
	        else
	        {
	            lKeyPressed = false;
	        }
	        break;
	}
}

function mouseDown()
{
	//Code
}

function degToRad(degrees)
{
	return(degrees * Math.PI / 180);
}
