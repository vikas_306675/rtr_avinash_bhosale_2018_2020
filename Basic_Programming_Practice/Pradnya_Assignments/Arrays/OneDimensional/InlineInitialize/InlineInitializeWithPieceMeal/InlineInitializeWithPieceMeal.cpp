#include<stdio.h>

int main(void)
{
	//Variable Declarations
	int iArray[] = { 9, 30, 6, 12, 98, 95, 20, 23, 2, 45 };
	int int_size;
	int iArray_size;
	int iArray_num_elements;

	float fArray[] = { 1.2f, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f };
	int float_size;
	int fArray_size;
	int fArray_num_elements;

	char cArray[] = { 'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P' };
	int char_size;
	int cArray_size;
	int cArray_num_elements;

	//Code

	//Integer Array
	printf("\nInLine Initialization and Piece Meal Display of Elements of Array - 'iArray':\n");
	printf("iArray[0] - 1st Element = %d\n", iArray[0]);
	printf("iArray[1] - 2nd Element = %d\n", iArray[1]);
	printf("iArray[2] - 3rd Element = %d\n", iArray[2]);
	printf("iArray[3] - 4th Element = %d\n", iArray[3]);
	printf("iArray[4] - 5th Element = %d\n", iArray[4]);
	printf("iArray[5] - 6th Element = %d\n", iArray[5]);
	printf("iArray[6] - 7th Element = %d\n", iArray[6]);
	printf("iArray[7] - 8th Element = %d\n", iArray[7]);
	printf("iArray[8] - 9th Element = %d\n", iArray[8]);
	printf("iArray[9] - 10th Element = %d\n", iArray[9]);

	int_size = sizeof(int);
	iArray_size = sizeof(iArray);
	iArray_num_elements = iArray_size / int_size;
	printf("\nSize of Data Type (int) = %d Bytes\n", int_size);
	printf("Number of Elements in the Array (iArray) = %d\n", iArray_num_elements);
	printf("Size of iArray (%d Elements * %d Bytes) = %d Bytes\n", iArray_num_elements, int_size, iArray_size);

	//Float Array
	printf("\nInLine Initialization and Piece Meal Display of Elements of Array - 'fArray':\n");
	printf("fArray[0] - 1st Element = %f\n", fArray[0]);
	printf("fArray[1] - 2nd Element = %f\n", fArray[1]);
	printf("fArray[2] - 3rd Element = %f\n", fArray[2]);
	printf("fArray[3] - 4th Element = %f\n", fArray[3]);
	printf("fArray[4] - 5th Element = %f\n", fArray[4]);
	printf("fArray[5] - 6th Element = %f\n", fArray[5]);
	printf("fArray[6] - 7th Element = %f\n", fArray[6]);
	printf("fArray[7] - 8th Element = %f\n", fArray[7]);

	float_size = sizeof(float);
	fArray_size = sizeof(fArray);
	fArray_num_elements = fArray_size / float_size;
	printf("\nSize of Data Type (float) = %d Bytes\n", float_size);
	printf("Number of Elements in the Array (fArray) = %d\n", fArray_num_elements);
	printf("Size of fArray (%d Elements * %d Bytes) = %d Bytes\n", fArray_num_elements, float_size, fArray_size);

	//Character Array
	printf("\nInLine Initialization and Piece Meal Display of Elements of Array - 'cArray':\n");
	printf("cArray[0] - 1st Element = %c\n", cArray[0]);
	printf("cArray[1] - 2nd Element = %c\n", cArray[1]);
	printf("cArray[2] - 3rd Element = %c\n", cArray[2]);
	printf("cArray[3] - 4th Element = %c\n", cArray[3]);
	printf("cArray[4] - 5th Element = %c\n", cArray[4]);
	printf("cArray[5] - 6th Element = %c\n", cArray[5]);
	printf("cArray[6] - 7th Element = %c\n", cArray[6]);
	printf("cArray[7] - 8th Element = %c\n", cArray[7]);
	printf("cArray[8] - 9th Element = %c\n", cArray[8]);
	printf("cArray[9] - 10th Element = %c\n", cArray[9]);
	printf("cArray[10] - 11th Element = %c\n", cArray[10]);
	printf("cArray[11] - 12th Element = %c\n", cArray[11]);
	printf("cArray[12] - 13th Element = %c\n", cArray[12]);

	char_size = sizeof(char);
	cArray_size = sizeof(cArray);
	cArray_num_elements = cArray_size / char_size;
	printf("\nSize of Data Type (char) = %d Bytes\n", char_size);
	printf("Number of Elements in the Array (cArray) = %d\n", cArray_num_elements);
	printf("Size of cArray (%d Elements * %d Bytes) = %d Bytes\n", cArray_num_elements, char_size, cArray_size);

	return 0;
}