package com.rtr.win_hello;

//Added packages by me
import androidx.appcompat.widget.AppCompatTextView; //For AppCompatTextView
import android.content.Context; //For context
import android.graphics.Color; //For Color
import android.view.Gravity; //For Gravity

public class MyView extends AppCompatTextView
{
    public MyView(Context drawingContext)
    {
	super(drawingContext);
	setTextColor(Color.rgb(0, 255, 0));
	setTextSize(60);
	setGravity(Gravity.CENTER);
	setText("Hello World!!");
    }
}
    