#include <stdio.h>
#include<string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declarations
	int MyStrlen(char[]);
	void MyStrcpy(char[], char[]);

	//Variable Declarations
	char chArray[MAX_STRING_LENGTH];
	int iStringLength, i, word_count = 0, space_count = 0;

	//Code
	printf("\nEnter a String: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);

	for (i = 0; i < iStringLength; i++)
	{
		switch (chArray[i])
		{
		case 32: //Ascii value for space character
			space_count++;
			break;

		default:
			break;
		}
	}

	word_count = space_count + 1;

	printf("\nString Entered: \n");
	printf("%s\n", chArray);

	printf("\nNumber of Spaces in the Input String = %d\n", space_count);
	printf("Number of Words in the Input String = %d\n", word_count);

	return 0;
}

int MyStrlen(char str[])
{
	//Variable Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return(string_length);
}
