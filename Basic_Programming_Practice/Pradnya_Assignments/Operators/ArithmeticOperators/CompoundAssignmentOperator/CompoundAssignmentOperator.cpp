#include<stdio.h>

int main(void)
{
	//variable declaration
	int a, b, x;

	//code
	printf("\nEnter a number: ");
	scanf("%d", &a);

	printf("\nEnter another number: ");
	scanf("%d", &b);

	x = a;
	a += b;
	printf("\nAddition of A = %d and B = %d gives result = %d\n", x, b, a);

	a = x;
	a -= b;
	printf("\nSubtraction of B = %d from A = %d gives result = %d\n", b, x, a);

	a = x;
	a *= b;
	printf("\nMultiplication of A = %d and B = %d gives result = %d\n", x, b, a);

	a = x;
	a /= b;
	printf("\nDivision of A = %d and B = %d gives quotient = %d\n", x, b, a);

	a = x;
	a %= b;
	printf("\nDivision of A = %d and B = %d gives remainder = %d\n", x, b, a);

	return 0;
}