#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j, c;

	//Code
	printf("\n");
	i = 0;
	do
	{
		j = 0;
		do
		{
			c = ((i & 0x8) == 0) ^ ((j & 0x8) == 0);

			if (c == 0)
				printf("  ");

			if (c == 1)
				printf("* ");

			j++;

		} while (j < 64);

		i++;
		printf("\n");

	} while (i < 64);

	printf("\nCheckered Board is Complete.\n");

	return 0;
}