#include <stdio.h>

int main(void)
{
	//Variable Declarations
	int length, breadth, area;

	struct MyPoint
	{
		int x;
		int y;
	};

	struct Rectangle
	{
		struct MyPoint point_01;
		struct MyPoint point_02;
	};

	struct Rectangle rect;

	//Code
	printf("\nEnter the Leftmost x-coordinate of the Rectangle: \n");
	scanf("%d", &rect.point_01.x);

	printf("\nEnter the Bottommost y-coordinate of the Rectangle: \n");
	scanf("%d", &rect.point_01.y);

	printf("\nEnter the Rightmost x-coordinate of the Rectangle: \n");
	scanf("%d", &rect.point_02.x);

	printf("\nEnter the Topmost y-coordinate of the Rectangle: \n");
	scanf("%d", &rect.point_02.y);

	length = rect.point_02.y - rect.point_01.y;

	if (length < 0)
	{
		length = length * -1;
	}

	breadth = rect.point_02.x - rect.point_01.x;
	
	if (breadth < 0)
	{
		breadth = breadth * -1;
	}

	area = length * breadth;

	printf("\nLength of the Rectangle = %d\n", length);
	printf("\nBreadth of the Rectangle = %d\n", breadth);
	printf("\nArea of the Rectangle = %d\n", area);

	return 0;
}
