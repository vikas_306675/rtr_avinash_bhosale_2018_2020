#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int iArray1[10];
	int iArray2[10];

	//Code
	//PieceMeal initialization of Array 1
	iArray1[0] = 10;
	iArray1[1] = 20;
	iArray1[2] = 30;
	iArray1[3] = 40;
	iArray1[4] = 50;
	iArray1[5] = 60;
	iArray1[6] = 70;
	iArray1[7] = 80;
	iArray1[8] = 90;
	iArray1[9] = 100;

	//Printing elements of Array 1
	printf("Piece-meal (Hardcoded) Assignment and Display of Elements of Array 'iArray1[]':\n");
	printf("1st Element of 'iArray1[]' =  %d\n", iArray1[0]);
	printf("2nd Element of 'iArray1[]' =  %d\n", iArray1[1]);
	printf("3rd Element of 'iArray1[]' =  %d\n", iArray1[2]);
	printf("4th Element of 'iArray1[]' =  %d\n", iArray1[3]);
	printf("5th Element of 'iArray1[]' =  %d\n", iArray1[4]);
	printf("6th Element of 'iArray1[]' =  %d\n", iArray1[5]);
	printf("7th Element of 'iArray1[]' =  %d\n", iArray1[6]);
	printf("8th Element of 'iArray1[]' =  %d\n", iArray1[7]);
	printf("9th Element of 'iArray1[]' =  %d\n", iArray1[8]);
	printf("10th Element of 'iArray1[]' =  %d\n", iArray1[9]);

	//PieceMeal initialization of Array 2
	printf("\nEnter the 1st Element of Araay 'iArray2[]':");
	scanf("%d", &iArray2[0]);
	printf("\nEnter the 2nd Element of Araay 'iArray2[]':");
	scanf("%d", &iArray2[1]);
	printf("\nEnter the 3rd Element of Araay 'iArray2[]':");
	scanf("%d", &iArray2[2]);
	printf("\nEnter the 4th Element of Araay 'iArray2[]':");
	scanf("%d", &iArray2[3]);
	printf("\nEnter the 5th Element of Araay 'iArray2[]':");
	scanf("%d", &iArray2[4]);
	printf("\nEnter the 6th Element of Araay 'iArray2[]':");
	scanf("%d", &iArray2[5]);
	printf("\nEnter the 7th Element of Araay 'iArray2[]':");
	scanf("%d", &iArray2[6]);
	printf("\nEnter the 8th Element of Araay 'iArray2[]':");
	scanf("%d", &iArray2[7]);
	printf("\nEnter the 9th Element of Araay 'iArray2[]':");
	scanf("%d", &iArray2[8]);
	printf("\nEnter the 10th Element of Araay 'iArray2[]':");
	scanf("%d", &iArray2[9]);
	

	//Printing elements of Array 2
	printf("Piece-meal (User Input) Assignment and Display of Elements of Array 'iArray2[]':\n");
	printf("1st Element of 'iArray2[]' =  %d\n", iArray2[0]);
	printf("2nd Element of 'iArray2[]' =  %d\n", iArray2[1]);
	printf("3rd Element of 'iArray2[]' =  %d\n", iArray2[2]);
	printf("4th Element of 'iArray2[]' =  %d\n", iArray2[3]);
	printf("5th Element of 'iArray2[]' =  %d\n", iArray2[4]);
	printf("6th Element of 'iArray2[]' =  %d\n", iArray2[5]);
	printf("7th Element of 'iArray2[]' =  %d\n", iArray2[6]);
	printf("8th Element of 'iArray2[]' =  %d\n", iArray2[7]);
	printf("9th Element of 'iArray2[]' =  %d\n", iArray2[8]);
	printf("10th Element of 'iArray2[]' =  %d\n", iArray2[9]);

	return 0;
}