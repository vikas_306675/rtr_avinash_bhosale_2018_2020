#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j;

	//Code
	printf("\nOuter Loop prints Odd Numbers between 1 to 10.\n");
	printf("\nInner Loop prints Even Numbers between 1 to 10 for Every Odd Numbers printed by outer loop.\n");

	for (i = 1; i <= 10; i++)
	{
		if (i % 2 != 0)
		{
			printf("i = %d\n", i);
			printf("--------\n");
			for (j = 1; j <= 10; j++)
			{
				if (j % 2 == 0)
				{
					printf("j = %d\n", j);
				}
				else
				{
					continue;
				}
			}
			printf("\n");
		}
		else
		{
			continue;
		}
	}

	printf("\n");

	return 0;
}