#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j, k;

	//Code
	printf("\n");

	i = 0;
	while (i <= 10)
	{
		printf("i = %d\n", i);
		printf("---------\n");
		j = 1;
		while (j <= 5)
		{
			printf("\tj = %d\n", j);
			printf("-----------\n");
			k = 1;
			while (k <= 3)
			{
				printf("k = %d\n", k);
				k++;
			}
			j++;
			printf("\n");
		}
		i++;
		printf("\n");
	}

	printf("All 3 loops end here\n");

	return 0;
}