#include <stdio.h>
#include<string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declaration
	int MyStrlen(char[]);

	//Variable Declarations
	char chArray[MAX_STRING_LENGTH]; 
	int iStringLength, i;
	int count_A = 0, count_E = 0, count_I = 0, count_O = 0, count_U = 0;

	//Code
	printf("\nEnter a String: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	printf("\nString Entered is: \n");
	printf("%s\n", chArray);

	iStringLength = MyStrlen(chArray);

	for (i = 0; i < iStringLength; i++)
	{
		switch (chArray[i])
		{
		case 'A':
		case 'a':
			count_A++;
			break;

		case 'E':
		case 'e':
			count_E++;
			break;

		case 'I':
		case 'i':
			count_I++;
			break;

		case 'O':
		case 'o':
			count_O++;
			break;

		case 'U':
		case 'u':
			count_U++;
			break;

		default:
			break;
		}
	}

	printf("\nIn the String entered by you, the Vowels and the number of their Occurences are as follows: \n");
	printf("\n'A' Has Occured = %d Times !\n", count_A);
	printf("\n'E' Has Occured = %d Times !\n", count_E);
	printf("\n'I' Has Occured = %d Times !\n", count_I);
	printf("\n'O' Has Occured = %d Times !\n", count_O);
	printf("\n'U' Has Occured = %d Times !\n", count_U);

	return 0;
}

int MyStrlen(char str[])
{
	//Varibale Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return(string_length);
}
