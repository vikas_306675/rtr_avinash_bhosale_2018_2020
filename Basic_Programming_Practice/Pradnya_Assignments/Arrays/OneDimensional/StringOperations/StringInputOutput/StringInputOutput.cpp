#include <stdio.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Variable Declarations 
	char chArray[MAX_STRING_LENGTH]; 

	//Code
	printf("\nEnter a String: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	printf("\nString Entered is: \n");
	printf("%s\n", chArray);
	
	return 0;
}

