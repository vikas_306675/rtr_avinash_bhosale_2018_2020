#include <stdio.h>
#include<string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declaration
	void MyStrcat(char[], char[]);

	//Variable Declarations 
	char chArray_One[MAX_STRING_LENGTH], chArray_Two[MAX_STRING_LENGTH];

	//Code
	printf("\nEnter First String: \n");
	gets_s(chArray_One, MAX_STRING_LENGTH);

	printf("\nEnter Second String: \n");
	gets_s(chArray_Two, MAX_STRING_LENGTH);

	printf("\n****** BEFORE CONCATENATION ******");
	printf("\nThe Original First String Entered ('chArray_One[]'): \n");
	printf("%s\n", chArray_One);

	printf("\nThe Original Second String Entered ('chArray_Two[]'): \n");
	printf("%s\n", chArray_Two);

	MyStrcat(chArray_One, chArray_Two);

	printf("\n****** AFTER CONCATENATION ******");
	printf("\n'chArray_One[]': \n");
	printf("%s\n", chArray_One);

	printf("\n'chArray_Two[]': \n");
	printf("%s\n", chArray_Two);

	return 0;
}

//Function to concatenate strings
void MyStrcat(char str_destination[], char str_source[])
{
	//Function Declaration
	int MyStrlen(char[]);

	//Variable Declarations 
	int iStringLength_Source = 0, iStringLength_Destination = 0, i, j;

	//Code
	iStringLength_Source = MyStrlen(str_source);
	iStringLength_Destination = MyStrlen(str_destination);

	for (i = iStringLength_Destination, j = 0; j < iStringLength_Source; i++, j++)
	{
		str_destination[i] = str_source[j];
	}

	str_destination[i] = '\0';
}

int MyStrlen(char str[])
{
	//Variable Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}

	return(string_length);
}
