#include <stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
} data;

int main(void)
{
	//Variable Declarations
	int i_size;
	int f_size;
	int d_size;
	int struct_MyData_size;

	//Code
	data.i = 45;
	data.f = 20.60;
	data.d = 3.147;

	printf("\nData Members of 'struct MyData' are: \n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);

	i_size = sizeof(data.i);
	f_size = sizeof(data.f);
	d_size = sizeof(data.d);

	printf("\n Sizes (in bytes) of Data Members 'struct MyData' are: \n");
	printf("Size of 'i' = %d bytes\n", i_size);
	printf("Size of 'f' = %d bytes\n", f_size);
	printf("Size of 'd' = %d bytes\n", d_size);

	struct_MyData_size = sizeof(struct MyData);

	printf("\nSize of 'struct MyData': %d bytes\n", struct_MyData_size);

	return 0;
}
