#include <stdio.h>
#include <string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Variable Declarations 
	char chArray[MAX_STRING_LENGTH];
	int iStringLength = 0;

	//Code
	printf("\nEnter a String : \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	printf("\nString Entered is: \n");
	printf("%s\n", chArray);

	//String Length
	iStringLength = strlen(chArray);
	printf("\nLength of the String is = %d Characters !!\n", iStringLength);

	return 0;
}
