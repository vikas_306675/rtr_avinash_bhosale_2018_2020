#include <stdio.h>

//Macros
#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	//Variable Declarations
	int iArray_2D[NUM_ROWS][NUM_COLUMNS];
	int iArray_1D[NUM_ROWS * NUM_COLUMNS];
	int i, j, num;

	//Code
	printf("\nEnter the Elements to fill up the Integer 2D Array: \n");
	for (i = 0; i < NUM_ROWS; i++)
	{
		printf("\nFor ROW NUMBER %d: \n", (i + 1));
		for (j = 0; j < NUM_COLUMNS; j++)
		{
			printf("\nEnter Element Number %d: \n", (j + 1));
			scanf("%d", &num);
			iArray_2D[i][j] = num;
		}
	}

	printf("\nTwo Dimensional Array of Integers: \n");
	for (i = 0; i < NUM_ROWS; i++)
	{
		printf("\n****** ROW %d ******\n", (i + 1));
		for (j = 0; j < NUM_COLUMNS; j++)
		{
			printf("iArray_2D[%d][%d] = %d\n", i, j, iArray_2D[i][j]);
		}
	}

	//Converting 2D Array into 1D Array
	for (i = 0; i < NUM_ROWS; i++)
	{
		for (j = 0; j < NUM_COLUMNS; j++)
		{
			iArray_1D[(i * NUM_COLUMNS) + j] = iArray_2D[i][j];
		}
	}

	printf("\nOne Dimensional Array of Integers: \n");
	for (i = 0; i < (NUM_ROWS * NUM_COLUMNS); i++)
	{
		printf("iArray_1D[%d]  = %d\n", i, iArray_1D[i]);
	}

	return 0;
}

