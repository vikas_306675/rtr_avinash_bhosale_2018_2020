#include <stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
} data = {40, 3.5f, 11.555, 'C' };

int main(void)
{
	//Code
	printf("\nData Members of 'struct MyData': \n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);
	printf("c = %c\n\n", data.c);

	return 0;
}
