#include <stdio.h>
#include<string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declarations
	int MyStrlen(char[]);
	void MyStrcpy(char[], char[]);

	//Variable Declarations
	char chArray_Original[MAX_STRING_LENGTH], chArray_VowelsReplaced[MAX_STRING_LENGTH];
	int iStringLength, i;

	//Code
	printf("\nEnter a String: \n");
	gets_s(chArray_Original, MAX_STRING_LENGTH);

	MyStrcpy(chArray_VowelsReplaced, chArray_Original);

	iStringLength = MyStrlen(chArray_VowelsReplaced);

	for (i = 0; i < iStringLength; i++)
	{
		switch (chArray_VowelsReplaced[i])
		{
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			chArray_VowelsReplaced[i] = '*';
			break;

		default:
			break;
		}
	}

	printf("\nString Entered: \n");
	printf("%s\n", chArray_Original);

	printf("\nString After Replacement of Vowels by '*': \n");
	printf("%s\n", chArray_VowelsReplaced);

	return 0;
}

//Function to calculate length of the string
int MyStrlen(char str[])
{
	//Variable Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}

	return(string_length);
}

//Function to copy the strings
void MyStrcpy(char str_destination[], char str_source[])
{
	//Function Declaration
	int MyStrlen(char[]);

	//Variable Declarations
	int iStringLength = 0, j;

	//Code
	iStringLength = MyStrlen(str_source);
	for (j = 0; j < iStringLength; j++)
	{
		str_destination[j] = str_source[j];
	}

	str_destination[j] = '\0';
}
