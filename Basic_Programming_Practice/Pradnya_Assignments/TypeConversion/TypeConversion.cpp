#include<stdio.h>

int main(void)
{
	//variable declaration
	int i, j;
	char ch1, ch2;

	int a, result_int;
	float f, result_float;

	int i_explicit;
	float f_explicit;

	//code

	//implicit type-casting
	i = 70;
	ch1 = i;
	printf("\nI = %d", i);
	printf("\nCharacter 1 (after ch_1 = i) = %c.\n", ch1);

	ch2 = 'Q';
	j = ch2;
	printf("\nCharacter 2 = %c", ch2);
	printf("\nJ after (j = ch_2) = %d.\n", j);

	a = 5;
	f = 7.8f;
	result_float = a + f;
	printf("\nInteger a = %d and Floating point number f = %f added gives Floating point sum = %f.\n", a, f, result_float);

	result_int = a + f;
	printf("\nInteger a =%d and Floating point number f = %f added gives Integer sum = %d.\n", a, f, result_int);

	//explicit type casting
	f_explicit = 30.121995f;
	i_explicit = (int)f_explicit;
	printf("\nFloating point number type casted explicity F = %f\n", f_explicit);
	printf("\nResultant Integer after explicit type casting of %f = %d.\n", f_explicit, i_explicit);

	return 0;
}