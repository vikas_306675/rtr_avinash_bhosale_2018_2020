#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int age;

	//Code
	printf("\nEnter your age: ");
	scanf("%d", &age);

	if (age >= 18)
	{
		printf("\nEntered the IF Block...\n");
		printf("\nYou are Eligible for Voting!\n");
	}
	else
	{
		printf("\nEntered the ELSE Block...\n");
		printf("\nYou are NOT Eligible for Voting!\n");
	}

	printf("\nBYE!!\n");

	return 0;
}