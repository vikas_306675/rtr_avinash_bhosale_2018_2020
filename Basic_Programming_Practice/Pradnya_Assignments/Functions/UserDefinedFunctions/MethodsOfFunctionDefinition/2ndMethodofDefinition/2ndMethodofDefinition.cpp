#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	//Function declaraton
	int MyAddition(void);

	//Variable Declaration
	int result;

	//Code
	result = MyAddition();

	printf("\nSum = %d\n", result);

	return 0;
}


//User Definied function with int as return value and no parameters
int MyAddition(void)
{
	//Variable Declaraton
	int a, b, sum;

	//Code
	printf("\nEnter Integer Value for A: ");
	scanf("%d", &a);

	printf("\nEnter Integer Value for B: ");
	scanf("%d", &b);

	sum = a + b;

	return(sum);
}