#include<stdio.h>

int main(void)
{
	//Variable Declarations
	int iArray[] = { 9, 30, 6, 12, 98, 95, 20, 23, 2, 45 };
	int int_size;
	int iArray_size;
	int iArray_num_elements;

	float fArray[] = { 1.2f, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f };
	int float_size;
	int fArray_size;
	int fArray_num_elements;

	char cArray[] = { 'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P' };
	int char_size;
	int cArray_size;
	int cArray_num_elements;

	int i;

	//Code
	//Integer Array
	printf("\nIn-Line Initialization and Using Loop to Display the Elements of the Array- 'iArrary' :\n");

	int_size = sizeof(int);
	iArray_size = sizeof(iArray);
	iArray_num_elements = iArray_size / int_size;

	for (i = 0; i < iArray_num_elements; i++)
	{
		printf("iArray[%d] (Elment %d) = %d\n", i, (i + 1), iArray[i]);
	}

	printf("\nSize of Data Type (int) = %d Bytes\n", int_size);
	printf("Number of Elements in the Array (iArray) = %d\n", iArray_num_elements);
	printf("Size of iArray (%d Elements * %d Bytes) = %d Bytes\n", iArray_num_elements, int_size, iArray_size);

	//Float Array
	float_size = sizeof(float);
	fArray_size = sizeof(fArray);
	fArray_num_elements = fArray_size / float_size;
	
	i = 0;
	while (i < fArray_num_elements)
	{
		printf("fArray[%d] (Element %d) = %f\n", i, (i + 1), fArray[i]);
		i++;
	}

	printf("\nSize of Data Type (float) = %d Bytes\n", float_size);
	printf("Number of Elements in the Array (fArray) = %d\n", fArray_num_elements);
	printf("Size of fArray (%d Elements * %d Bytes) = %d Bytes\n", fArray_num_elements, float_size, fArray_size);

	//Character Array
	char_size = sizeof(char);
	cArray_size = sizeof(cArray);
	cArray_num_elements = cArray_size / char_size;

	i = 0;
	do
	{
		printf("cArray[%d] (Element %d) = %d\n", i, (i + 1), cArray[i]);
		i++;
	} while (i < cArray_num_elements);

	printf("\nSize of Data Type (char) = %d Bytes\n", char_size);
	printf("Number of Elements in the Array (cArray) = %d\n", cArray_num_elements);
	printf("Size of cArray (%d Elements * %d Bytes) = %d Bytes\n", cArray_num_elements, char_size, cArray_size);

	return 0;
}