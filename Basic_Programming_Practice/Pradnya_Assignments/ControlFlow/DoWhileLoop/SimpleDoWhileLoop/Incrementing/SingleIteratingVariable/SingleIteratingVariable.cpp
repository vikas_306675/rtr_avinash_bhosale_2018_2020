#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i;

	//Code
	printf("\nPrinting Digits 1 to 10 - ");

	i = 1;
	do
	{
		printf("\n%d", i);
		i++;
	} while (i <= 10);

	printf("\nDo-While Loop Completed.\n");

	return 0;
}