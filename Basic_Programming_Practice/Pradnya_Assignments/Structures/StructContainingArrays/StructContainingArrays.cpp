#include <stdio.h>
#include<string.h>

//Macros
#define INT_ARRAY_SIZE 10
#define FLOAT_ARRAY_SIZE 5
#define CHAR_ARRAY_SIZE 26
#define NUM_STRINGS 10
#define MAX_CHARACTERS_PER_STRING 20
#define ALPHABET_BEGINNING 65 //ASCII for Character 'A'

struct MyDataOne
{
	int iArray[INT_ARRAY_SIZE];
	float fArray[FLOAT_ARRAY_SIZE];
};

struct MyDataTwo
{
	char cArray[CHAR_ARRAY_SIZE];
	char strArray[NUM_STRINGS][MAX_CHARACTERS_PER_STRING];
};

int main(void)
{
	//Variable Declarations
	struct MyDataOne data_one;
	struct MyDataTwo data_two;
	int i;

	//Code
	data_one.fArray[0] = 0.1f;
	data_one.fArray[1] = 1.2f;
	data_one.fArray[2] = 2.3f;
	data_one.fArray[3] = 3.4f;
	data_one.fArray[4] = 4.5f;

	printf("\nEnter %d Integers: \n", INT_ARRAY_SIZE);
	for (i = 0; i < INT_ARRAY_SIZE; i++)
	{
		scanf("%d", &data_one.iArray[i]);
	}

	for (i = 0; i < CHAR_ARRAY_SIZE; i++)
	{
		data_two.cArray[i] = (char)(i + ALPHABET_BEGINNING);
	}

	strcpy(data_two.strArray[0], "Welcome!!");
	strcpy(data_two.strArray[1], "this");
	strcpy(data_two.strArray[2], "is");
	strcpy(data_two.strArray[3], "ASTROMEDICOMP'S");
	strcpy(data_two.strArray[4], "Real");
	strcpy(data_two.strArray[5], "Time");
	strcpy(data_two.strArray[6], "Rendering");
	strcpy(data_two.strArray[7], "Batch");
	strcpy(data_two.strArray[8], "of");
	strcpy(data_two.strArray[9], "2018-19-20!!");

	printf("\nMembers of  the 'struct DataOne' along with their Assigned Values: \n");
	printf("\nInteger Array(data_one.iArray[]): \n");
	for (i = 0; i < INT_ARRAY_SIZE; i++)
	{
		printf("data_one.iArray[%d] = %d\n", i, data_one.iArray[i]);
	}
	printf("\nFloating Point Array(data_one.fArray[]): \n");
	for (i = 0; i < FLOAT_ARRAY_SIZE; i++)
	{
		printf("data_one.fArray[%d] = %f\n", i, data_one.fArray[i]);
	}

	printf("\nMembers of the 'struct DataTwo' along with their Assigned Values: \n");
	printf("\nCharacter Array(data_two.cArray[]): \n");
	for (i = 0; i < CHAR_ARRAY_SIZE; i++)
	{
		printf("data_two.cArray[%d] = %c\n", i, data_two.cArray[i]);
	}
	printf("\nString Array(data_two.strArray[]): \n");
	for (i = 0; i < NUM_STRINGS; i++)
	{
		printf("%s ", data_two.strArray[i]);
	}
	
	return 0;
}
