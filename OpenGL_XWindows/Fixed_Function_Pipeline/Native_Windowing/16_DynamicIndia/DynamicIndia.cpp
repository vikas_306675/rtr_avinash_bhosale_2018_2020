#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>
#include<math.h>
#define _USE_MATH_DEFINES 1

//Namespaces
using namespace std;

//Global Variables
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWinWidth = 800;
int giWinHeight = 600;
char keys[26];
static GLXContext gGLXContext;

//Entry Point Function
int main(void)
{
	//Function Declaration
	void CreateWindow(void);
	void uninitialize(void);
	void initialize(void);
	void ToggleFullscreen(void);
	void resize(int, int);
	void display(void);

	//Variable Declarations
	int winWidth = giWinWidth;
	int winHeight = giWinHeight;
	bool bDone = false;

	//Code
	CreateWindow();
	initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							uninitialize();
							exit(0);
						default:
							break;
					}

					XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
					switch(keys[0])
					{
						case 'F':
						case 'f':
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
					}
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone = true;
					uninitialize();
					exit(0);
				default:
					break;
			}
		}
		display();
	}
	
	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	//Function Declaration
	void uninitialize(void);

	//Variable Declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[] = {GLX_RGBA, GLX_DOUBLEBUFFER, True, GLX_RED_SIZE, 8, GLX_GREEN_SIZE, 8, GLX_ALPHA_SIZE, 8, GLX_DEPTH_SIZE, 24, None};

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("Error: Unable to Open X Display.\nExiting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("Error: Unable to create Visual Info.\nExiting Now...\n");
		uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
 	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), 0, 0, giWinWidth, giWinHeight, 0, gpXVisualInfo->depth, InputOutput, gpXVisualInfo->visual, styleMask, &winAttribs);

	if(!gWindow)
	{
		printf("Error: Failed to Create Main Window.\nExiting Now...\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "OpenGL XWindows Window - Avinash Bhosale");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);
}

void initialize(void)
{
	//Function Declaration
	void uninitialize(void);
	void resize(int, int);
	
	//Code
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, True);

	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	//Clear the Screen with OpenGL Color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Warmup call for resize
	resize(giWinWidth, giWinHeight);
}

void resize(int width, int height)
{
	//Code
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	//Function Declaration
	void drawIL(void);
	void drawN(void);
	void drawD(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void drawIR(void);
	void drawA(void);
	void drawPlane(void);
	void drawExhaust(void);

	//Variable Declaration
	static GLfloat plane_x = -2.40f;
	static GLfloat translateILx = -2.0f;
	static GLfloat translateNy = 2.5f;
	static GLfloat translateIRy = -2.0f;
	static GLfloat translateAx = 2.0f;
	static GLfloat colorSaffron1 = 0.0f;
	static GLfloat colorSaffron2 = 0.0f;
	static GLfloat colorSaffron3 = 0.0f;
	static GLfloat colorGreen1 = 0.0f;
	static GLfloat colorGreen2 = 0.0f;
	static GLfloat colorGreen3 = 0.0f;
	static GLfloat plane_rotation = -90.0f;
	static GLsizei plane_radius = 0;
	static GLfloat exhaust_x1 = -0.75f, exhaust_x2 = -0.70f;
	GLsizei i = 1000;
	GLfloat plane_angle = (M_PI / 2) * plane_radius / i;
	static GLfloat plane_x2 = 0.0f;
	static GLsizei counter = 0.0f;

	//Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//Bringing I from Left
	glTranslatef(translateILx, 0.0f, -3.0f);
	glLineWidth(5.0f);
	drawIL();

	//Bringing N from Top
	glLoadIdentity();
	glTranslatef(0.0f, translateNy, -3.0f);
	drawN();
	
	//Fading In the Letter D using Color Variables
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	drawD(colorSaffron1, colorSaffron2, colorSaffron3, colorGreen1, colorGreen2, colorGreen3);

	//Bringing I from the Bottom
	glLoadIdentity();
	glTranslatef(0.0f, translateIRy, -3.0f);
	drawIR();

	//Bringing A from the Right
	glLoadIdentity();
	glTranslatef(translateAx, 0.0f, -3.0f);
	drawA();

	//Bringing the Horizontal Plane from Left side
	glLoadIdentity();
	glTranslatef(plane_x, 0.0f, -2.0f);
	drawPlane();

	//Drawing the Exhaust of the Tri Color
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -2.0f);
	if (plane_x >= -0.60f)
	{

		glLineWidth(5.0f);
		glBegin(GL_LINES);

		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(exhaust_x1, 0.01f, 0.0f);
		glColor3f(1.00f, 0.60f, 0.20f);
		glVertex3f(exhaust_x2, 0.01f, 0.0f);

		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(exhaust_x1, 0.0f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(exhaust_x2, 0.0f, 0.0f);

		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(exhaust_x1, -0.01f, 0.0f);
		glColor3f(0.07f, 0.53f, 0.027f);
		glVertex3f(exhaust_x2, -0.01f, 0.0f);
		glEnd();
	}

	//Bringing in the Planes from Top and Bottom Left corners and rotating them on Z axis
	glLoadIdentity();
	glTranslatef(-0.6f, 1.0f, 0.0f);
	glTranslatef(-cos(plane_angle) + plane_x2, -sin(plane_angle), -2.0f);
	glRotatef(plane_rotation, 0.0f, 0.0f, 1.0f);
	drawPlane();

	glLoadIdentity();
	glTranslatef(-0.6f, -1.0f, 0.0f);
	glTranslatef(-cos(plane_angle) + plane_x2, sin(plane_angle), -2.0f);
	glRotatef(-plane_rotation, 0.0f, 0.0f, 1.0f);
	drawPlane();

	//Swapping Buffers
	glXSwapBuffers(gpDisplay, gWindow);

	//Condition to translate I coming in from Left side
	if (translateILx <= 0.0f)
	{
		translateILx = translateILx + 0.005f;
	}

	//Condition to translate A coming in from Right side
	if (translateILx >= 0.0f)
	{
		if (translateAx >= 00.f)
		{
			translateAx = translateAx - 0.005f;
		}
	}

	//Condition to translate N coming in from Top side
	if (translateAx <= 0.0f)
	{
		if (translateNy >= 0.0f)
		{
			translateNy = translateNy - 0.005f;
		}
	}

	//Condition to translate I coming in from Bottom side
	if (translateNy <= 0.0f)
	{
		if (translateIRy <= 0.0f)
		{
			translateIRy = translateIRy + 0.005f;
		}
	}

	//Condition to Fade In D using Color variables
	if (translateIRy >= 0.0f)
	{
		if (colorSaffron1 <= 1.0f)
		{
			colorSaffron1 = colorSaffron1 + 0.002f;
		}

		if (colorSaffron2 <= 0.60f)
		{
			colorSaffron2 = colorSaffron2 + 0.001f;
		}

		if (colorSaffron3 <= 0.20f)
		{
			colorSaffron3 = colorSaffron3 + 0.003f;
		}

		if (colorGreen1 <= 0.07f)
		{
			colorGreen1 = colorGreen1 + 0.010f;
		}

		if (colorGreen2 <= 0.53f)
		{
			colorGreen2 = colorGreen2 + 0.002f;
		}

		if (colorGreen3 <= 0.027f)
		{
			colorGreen3 = colorGreen3 + 0.010f;
		}
	}

	//Condition to translate the plane coming in Horizontally
	if (colorSaffron1 >= 1.0f)
	{
		if (plane_x <= 0.65f)
		{
			plane_x = plane_x + 0.00361f;
		}

		if (plane_radius < i)
		{
			plane_radius = plane_radius + 2;
			if (plane_rotation <= 0.0f)
			{
				plane_rotation = plane_rotation + 0.2f;
			}
		}

		if (counter <= 3400)
		{
			counter = counter + 1;
		}
	}

	//Condition to translate the Planes coming in from an angle to move Horizontally after the angle ends on the x-axis
	if (plane_radius >= i)
	{
		if (plane_x <= 0.65f)
		{
			plane_x2 = plane_x2 + 0.00361f;
		}
	}

	if (counter >= 3400)
	{
		
		if (plane_x <= 2.0f)
		{
			plane_x = plane_x + 0.00361f;
		}

		if (plane_radius < 2 * i)
			{
				plane_radius = plane_radius + 2;
			}

		if (plane_x > 0.65f)
		{
				plane_x2 = plane_x2 + 0.00005f;
		}

		plane_rotation = plane_rotation + 0.2f;

	}
	//Condition to start drawing the exhaust of Tri Color
	if (plane_x >= -0.60f)
	{
		if (exhaust_x2 <= 0.43f)
		{
			exhaust_x2 = exhaust_x2 + 0.00361f;
		}
	}

	//Condition to Fade out the Tri Color to a certain point on A
	if (exhaust_x2 >= 0.43f)
	{
		if (exhaust_x1 <= 0.34f)
		{
			exhaust_x1 = exhaust_x1 + 0.00361f;
		}
	}
}

//Draw Left I
void drawIL(void)
{
	//Drawing Left I of INDIA
	glBegin(GL_LINES);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.60f, 0.60f, 0.0f);
	glVertex3f(-0.80f, 0.60f, 0.0f);
	glVertex3f(-0.70f, 0.60f, 0.0f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(-0.70f, -0.60f, 0.0f);
	glVertex3f(-0.80f, -0.60f, 0.0f);
	glVertex3f(-0.60f, -0.60f, 0.0f);
	glEnd();
}

//Draw N
void drawN(void)
{
	//Drawing N of INDIA
	glBegin(GL_LINES);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.50f, 0.60f, 0.00f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(-0.50f, -0.60f, 0.0f);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.50f, 0.60f, 0.00f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(-0.30f, -0.60f, 0.0f);
	glVertex3f(-0.30f, -0.60f, 0.0f);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.30f, 0.60f, 0.0f);
	glEnd();
}

//Draw D
void drawD(GLfloat color1, GLfloat color2, GLfloat color3, GLfloat color4, GLfloat color5, GLfloat color6)
{
	//Drawing D of INDIA
	glBegin(GL_LINES);
	glColor3f(color1, color2, color3);
	glVertex3f(-0.10f, 0.60f, 0.00f);
	glColor3f(color4, color5, color6);
	glVertex3f(-0.10f, -0.60f, 0.0f);
	glColor3f(color1, color2, color3);
	glVertex3f(-0.15f, 0.60f, 0.00f);
	glVertex3f(0.10f, 0.60f, 0.00f);
	glVertex3f(0.10f, 0.60f, 0.00f);
	glColor3f(color4, color5, color6);
	glVertex3f(0.10f, -0.60f, 0.0f);
	glVertex3f(0.10f, -0.60f, 0.0f);
	glVertex3f(-0.15f, -0.60f, 0.00f);
	glEnd();
}

//Draw Right R
void drawIR(void)
{
	//Drawing Right I of INDIA
	glBegin(GL_LINES);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(0.20f, 0.60f, 0.0f);
	glVertex3f(0.40f, 0.60f, 0.0f);
	glVertex3f(0.30f, 0.60f, 0.0f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(0.30f, -0.60f, 0.0f);
	glVertex3f(0.20f, -0.60f, 0.0f);
	glVertex3f(0.40f, -0.60f, 0.0f);
	glEnd();
}

//Draw A
void drawA(void)
{
	//Drawing A of INDIA
	glBegin(GL_LINES);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(0.70f, -0.60f, 0.0f);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(0.60f, 0.60f, 0.0f);
	glVertex3f(0.60f, 0.60f, 0.0f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(0.50f, -0.60f, 0.0f);
	glEnd();
}

//Draw Planes
void drawPlane(void)
{
	glBegin(GL_TRIANGLES);
	glColor3f(0.726f, 0.882f, 0.929f);
	glVertex3f(0.0f, 0.025f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glEnd();
	glBegin(GL_TRIANGLES);
	glColor3f(0.726f, 0.882f, 0.929f);
	glVertex3f(0.0f, -0.025f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glEnd();
	glBegin(GL_TRIANGLES);
	glColor3f(0.726f, 0.882f, 0.929f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(-0.075f, 0.025f, 0.0f);
	glVertex3f(-0.075f, -0.025f, 0.0f);
	glBegin(GL_TRIANGLES);
	glColor3f(0.726f, 0.882f, 0.929f);
	glVertex3f(0.00f, -0.05f, 0.0f);
	glVertex3f(-0.025f, 0.0f, 0.0f);
	glVertex3f(0.025f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.05f, 0.0f);
	glVertex3f(-0.025f, 0.0f, 0.0f);
	glVertex3f(0.025f, 0.0f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.010f, 0.013f, 0.0f);
	glVertex3f(-0.010f, -0.013f, 0.0f);

	glVertex3f(0.002f, -0.013f, 0.0f);
	glVertex3f(-0.001f, 0.013f, 0.0f);
	glVertex3f(-0.001f, 0.013f, 0.0f);
	glVertex3f(-0.005f, -0.013f, 0.0f);

	glVertex3f(0.012f, 0.013f, 0.0f);
	glVertex3f(0.008f, 0.013f, 0.0f);
	glVertex3f(0.011f, 0.004f, 0.0f);
	glVertex3f(0.008f, 0.004f, 0.0f);
	glVertex3f(0.008f, 0.013f, 0.0f);
	glVertex3f(0.008f, -0.013f, 0.0f);
	glEnd();
}

void ToggleFullscreen(void)
{
	//Variable Declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//Code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
	XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), False, StructureNotifyMask, &xev);
}

void uninitialize(void)
{

	GLXContext currentGLXContext = glXGetCurrentContext();

	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}

}
	
