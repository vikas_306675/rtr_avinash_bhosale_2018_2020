#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i;

	//Code
	printf("\nPrinting Digits from 10 to 1 - ");

	i = 10;
	while(i > 0)
	{
		printf("\n%d", i);
		i--;
	}

	printf("\nWhile Loop Completed.\n");

	return 0;
}