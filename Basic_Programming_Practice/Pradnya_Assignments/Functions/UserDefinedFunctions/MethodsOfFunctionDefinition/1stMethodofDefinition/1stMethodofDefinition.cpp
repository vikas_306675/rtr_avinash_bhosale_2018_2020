#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	//Function declaraton
	void MyAddition(void);

	//Code
	MyAddition();

	return 0;
}


//User Definied function with no return value and no parameters
void MyAddition(void)
{
	//Variable Declaraton
	int a, b, sum;

	//Code
	printf("\nEnter Integer Value for A: ");
	scanf("%d", &a);

	printf("\nEnter Integer Value for B: ");
	scanf("%d", &b);

	sum = a + b;

	printf("\nSum of %d and %d = %d\n", a, b, sum);
}