//Global Variables
var canvas_ab=null;
var gl=null;
var bFullscreen_ab=false;
var canvas_original_width_ab;
var canvas_original_height_ab;

const WebGLMacros = 
{
AMC_ATTRIBUTE_VERTEX:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject_pv_ab;
var fragmentShaderObject_pv_ab;
var vertexShaderObject_pf_ab;
var fragmentShaderObject_pf_ab;
var shaderProgramObject_pv_ab;
var shaderProgramObject_pf_ab;


var lightAmbient_ab = [0.0, 0.0, 0.0];
var lightDiffuse_ab = [1.0, 1.0, 1.0];
var lightSpecular_ab = [1.0, 1.0, 1.0];
var lightPosition_ab = [100.0, 100.0, 100.0, 1.0];

var materialAmbient_ab = [0.0, 0.0, 0.0];
var materialDiffuse_ab = [1.0, 1.0, 1.0];
var materialSpecular_ab = [1.0, 1.0, 1.0];
var materialShininess_ab = 50.0;

var sphere_ab = null;

var modelMatrixUniform_pv, viewMatrixUniform_pv, projectionMatrixUniform_pv;
var modelMatrixUniform_pf, viewMatrixUniform_pf, projectionMatrixUniform_pf;
var perspectiveProjectionMatrix;

var laUniform_pv_ab, lsUniform_pv_ab, ldUniform_pv_ab, lightPositionUniform_pv_ab;
var kaUniform_pv_ab, ksUniform_pv_ab, kdUniform_pv_ab, materialShininessUniform_pv_ab;
var LKeyPressedUniform_pv_ab;

var laUniform_pf_ab, lsUniform_pf_ab, ldUniform_pf_ab, lightPositionUniform_pf_ab;
var kaUniform_pf_ab, ksUniform_pf_ab, kdUniform_pf_ab, materialShininessUniform_pf_ab;
var LKeyPressedUniform_pf_ab;


var lKeyPressed = false;
var shaderKeypress = 1;


//To start Animation
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop Animation
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

//Onload function
function main()
{
	//Get canvas element
	canvas_ab = document.getElementById("AMC");
	if(!canvas_ab)
		console.log("Obtaining canvas_ab Failed\n");
	else
		console.log("Obtaining canvas_ab Succeeded\n");

	canvas_original_width_ab=canvas_ab.width;
	canvas_original_height_ab=canvas_ab.height;

	//Register event handlers
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//Initialize WebGL
	init();

	//Start Drawing
	resize();
	draw();
}

function toggleFullscreen()
{
	//Code
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	//If Fullscreen is not enabled
	if(fullscreen_element==null)
	{
		if(canvas_ab.requestFullscreen)
			canvas_ab.requestFullscreen();
		else if(canvas_ab.mozRequestFullScreen)
			canvas_ab.mozRequestFullScreen();
		else if (canvas_ab.webkitRequestFullscreen)
			canvas_ab.webkitRequestFullscreen();
		else if(canvas_ab.msRequestFullscreen)
			canvas_ab.msRequestFullscreen();
		bFullscreen_ab=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen_ab=false;
	}
}

function init()
{
    //Code

	//Get the WebGL 2.0 Context
	gl = canvas_ab.getContext("webgl2"); //webgl1 in case of mobile browser

	//If Getting Context Fails
	if(gl == null)
	{
		console.log("Failed to get the Rendering Context for WebGL");
		return;
	}
	
	gl.viewportWidth = canvas_ab.width;
	gl.viewportHeight = canvas_ab.height;

    //Vertex Shader Per Vertex
	var vertexShaderSourceCode_pv =
	"#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_m_matrix;" +
    "uniform mat4 u_v_matrix;" +
    "uniform mat4 u_p_matrix;" +
    "uniform mediump int u_lIsPressed;" +
    "uniform vec3 u_la;" +
    "uniform vec3 u_ld;" +
    "uniform vec3 u_ls;" +
    "uniform vec3 u_ka;" +
    "uniform vec3 u_kd;" +
    "uniform vec3 u_ks;" +
    "uniform vec4 u_light_position;" +
    "uniform float u_material_shininess;" +
    "out vec3 phong_ads_light;" +
    "void main(void)" +
    "{" +
    "if(u_lIsPressed==1)" +
    "{" +
    "vec4 eye_coordinates = u_v_matrix * u_m_matrix * vPosition;" +
    "vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);" +
    "vec3 lightDirection = normalize(vec3(u_light_position - eye_coordinates));" +
    "float tn_dot_ld = max(dot(lightDirection, tNorm), 0.0);" +
    "vec3 reflectionVector = reflect(-lightDirection, tNorm);" +
    "vec3 viewerVector = normalize(-eye_coordinates.xyz);" +
    "vec3 ambient = u_la * u_ka;" +
    "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" +
    "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, viewerVector), 0.0), u_material_shininess);" +
    "phong_ads_light = ambient + diffuse + specular;" +
    "}" +
    "else" +
    "{" +
    "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
    "}" +
    "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
    "}";

	vertexShaderObject_pv_ab = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_pv_ab, vertexShaderSourceCode_pv);
	gl.compileShader(vertexShaderObject_pv_ab);

	if (gl.getShaderParameter(vertexShaderObject_pv_ab, gl.COMPILE_STATUS) == false)
	{
	    var error = gl.getShaderInfoLog(vertexShaderObject_pv_ab);
	    if (error.length > 0) {
	        console.log("Vertex Shader Error:\n");
	        alert(error);
	        uninitialize();
	    }
	}

    //Fragment Shader Per Vertex
	var fragmentShaderSourceCode_pv =
	"#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec3 phong_ads_light;" +
    "out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "FragColor = vec4(phong_ads_light, 1.0);" +
    "}";

	fragmentShaderObject_pv_ab = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_pv_ab, fragmentShaderSourceCode_pv);
	gl.compileShader(fragmentShaderObject_pv_ab);

	if (gl.getShaderParameter(fragmentShaderObject_pv_ab, gl.COMPILE_STATUS) == false)
	{
	    var error = gl.getShaderInfoLog(fragmentShaderObject_pv_ab);
	    if (error.length > 0) {
	        console.log("Fragment Shader Error:\n");
	        alert(error);
	        uninitialize();
	    }
	}

    //Shader Program Per Vertex
	shaderProgramObject_pv_ab = gl.createProgram();
	gl.attachShader(shaderProgramObject_pv_ab, vertexShaderObject_pv_ab);
	gl.attachShader(shaderProgramObject_pv_ab, fragmentShaderObject_pv_ab);

    //Pre-Linking of the Shader Program Object with Vertex Shader Attributes
	gl.bindAttribLocation(shaderProgramObject_pv_ab, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_pv_ab, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

    //Linking
	gl.linkProgram(shaderProgramObject_pv_ab);

	if (gl.getProgramParameter(shaderProgramObject_pv_ab, gl.LINK_STATUS))
	{
	    var error = gl.getProgramInfoLog(shaderProgramObject_pv_ab);
	    if (error.length > 0) {
	        alert(error);
	        uninitialize();
	    }
	}

    //Get MVP Uniform location
	modelMatrixUniform_pv = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_m_matrix");
	viewMatrixUniform_pv = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_v_matrix");
	projectionMatrixUniform_pv = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_p_matrix");
	LKeyPressedUniform_pv_ab = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_lIsPressed");
	laUniform_pv_ab = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_la");
	ldUniform_pv_ab = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_ld");
	lsUniform_pv_ab = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_ls");
	kaUniform_pv_ab = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_ka");
	kdUniform_pv_ab = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_kd");
	ksUniform_pv_ab = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_ks");
	lightPositionUniform_pv_ab = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_light_position");
	materialShininessUniform_pv_ab = gl.getUniformLocation(shaderProgramObject_pv_ab, "u_material_shininess");

	//Vertex Shader Per Fragment
	var vertexShaderSourceCode_pf = 
	"#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_m_matrix;" +
    "uniform mat4 u_v_matrix;" +
    "uniform mat4 u_p_matrix;" +
    "uniform vec4 u_light_position;" +
    "out vec3 tNorm;" +
    "out vec3 lightDirection;" +
    "out vec3 viewerVector;" +
    "void main(void)" +
    "{" +
    "vec4 eye_coordinates = u_v_matrix * u_m_matrix * vPosition;" +
    "tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;" +
    "lightDirection = vec3(u_light_position - eye_coordinates);" +
    "viewerVector = -eye_coordinates.xyz;" +
    "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
    "}";
	
	vertexShaderObject_pf_ab = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_pf_ab, vertexShaderSourceCode_pf);
	gl.compileShader(vertexShaderObject_pf_ab);
	
	if (gl.getShaderParameter(vertexShaderObject_pf_ab, gl.COMPILE_STATUS) == false)
	{
	    var error = gl.getShaderInfoLog(vertexShaderObject_pf_ab);
		if(error.length > 0)
		{
		    console.log("Vertex Shader Error:\n");
			alert(error);
			uninitialize();
		}
	}

	//Fragment Shader Per Fragment
	var fragmentShaderSourceCode_pf = 
	"#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec3 tNorm;" +
    "in vec3 lightDirection;" +
    "in vec3 viewerVector;" +
    "uniform mediump int u_lIsPressed;" +
    "uniform vec3 u_la;" +
    "uniform vec3 u_ld;" +
    "uniform vec3 u_ls;" +
    "uniform vec3 u_ka;" +
    "uniform vec3 u_kd;" +
    "uniform vec3 u_ks;" +
    "uniform float u_material_shininess;" +
    "out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "if(u_lIsPressed == 1)" +
    "{" +
    "vec3 normalizedTNorm = normalize(tNorm);" +
	"vec3 normalizedLightDirection = normalize(lightDirection);" +
	"vec3 normalizedViewerVector = normalize(viewerVector);" +
	"float tn_dot_ld = max(dot(normalizedLightDirection, normalizedTNorm), 0.0);" +
	"vec3 reflectionVector = reflect(-normalizedLightDirection, normalizedTNorm);" +
	"vec3 ambient = u_la * u_ka;" +
	"vec3 diffuse = u_ld * u_kd * tn_dot_ld;" +
	"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalizedViewerVector), 0.0), u_material_shininess);" +
	"vec3 phong_ads_light = ambient + diffuse + specular;" +
    "FragColor = vec4(phong_ads_light, 1.0);" +
    "}" +
    "else" +
    "{" +
    "FragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
    "}" +
    "}";
	
	fragmentShaderObject_pf_ab = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_pf_ab, fragmentShaderSourceCode_pf);
	gl.compileShader(fragmentShaderObject_pf_ab);
	
	if (gl.getShaderParameter(fragmentShaderObject_pf_ab, gl.COMPILE_STATUS) == false)
	{
	    var error = gl.getShaderInfoLog(fragmentShaderObject_pf_ab);
		if(error.length > 0)
		{
		    console.log("Fragment Shader Error:\n");
			alert(error);
			uninitialize();
		}
	}

	//Shader Program Per Fragment
	shaderProgramObject_pf_ab = gl.createProgram();
	gl.attachShader(shaderProgramObject_pf_ab, vertexShaderObject_pf_ab);
	gl.attachShader(shaderProgramObject_pf_ab, fragmentShaderObject_pf_ab);

	//Pre-Linking of the Shader Program Object with Vertex Shader Attributes
	gl.bindAttribLocation(shaderProgramObject_pf_ab, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_pf_ab, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");
	
	//Linking
	gl.linkProgram(shaderProgramObject_pf_ab);
	
	if (gl.getProgramParameter(shaderProgramObject_pf_ab, gl.LINK_STATUS))
	{
	    var error = gl.getProgramInfoLog(shaderProgramObject_pf_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Get MVP Uniform location
	modelMatrixUniform_pf = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_m_matrix");
	viewMatrixUniform_pf = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_v_matrix");
	projectionMatrixUniform_pf = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_p_matrix");
	LKeyPressedUniform_pf_ab = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_lIsPressed");
	laUniform_pf_ab = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_la");
	ldUniform_pf_ab = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_ld");
	lsUniform_pf_ab = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_ls");
	kaUniform_pf_ab = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_ka");
	kdUniform_pf_ab = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_kd");
	ksUniform_pf_ab = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_ks");
	lightPositionUniform_pf_ab = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_light_position");
	materialShininessUniform_pf_ab = gl.getUniformLocation(shaderProgramObject_pf_ab, "u_material_shininess");

    //sphere_ab
	sphere_ab = new Mesh();
	makeSphere(sphere_ab, 2.0, 40, 40);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	//Set the Clear Color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	//Initialize Projection Matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//Code
	if(bFullscreen_ab == true)
	{
		canvas_ab.width=window.innerWidth;
		canvas_ab.height=window.innerHeight;
	}
	else
	{
		canvas_ab.width=canvas_original_width_ab;
		canvas_ab.height=canvas_original_height_ab;
	}

	//Set the viewport
	gl.viewport(0, 0, canvas_ab.width, canvas_ab.height);

	//Perspective Projection
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas_ab.width)/parseFloat(canvas_ab.height), 0.1, 100.0);
	
}

function draw()
{
	//Code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();

	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	mat4.identity(viewMatrix);

	if (shaderKeypress == 1)
	{
	    gl.useProgram(shaderProgramObject_pv_ab);

	    gl.uniformMatrix4fv(modelMatrixUniform_pv, false, modelMatrix);
	    gl.uniformMatrix4fv(viewMatrixUniform_pv, false, viewMatrix);
	    gl.uniformMatrix4fv(projectionMatrixUniform_pv, false, perspectiveProjectionMatrix);


	    if (lKeyPressed == true)
	    {
	        gl.uniform1i(LKeyPressedUniform_pv_ab, 1);
	        gl.uniform3fv(laUniform_pv_ab, lightAmbient_ab);
	        gl.uniform3fv(ldUniform_pv_ab, lightDiffuse_ab);
	        gl.uniform3fv(lsUniform_pv_ab, lightSpecular_ab);
	        gl.uniform3fv(kaUniform_pv_ab, materialAmbient_ab);
	        gl.uniform3fv(kdUniform_pv_ab, materialDiffuse_ab);
	        gl.uniform3fv(ksUniform_pv_ab, materialSpecular_ab);
	        gl.uniform1f(materialShininessUniform_pv_ab, materialShininess_ab);
	        gl.uniform4fv(lightPositionUniform_pv_ab, lightPosition_ab);
	    }
	    else {
	        gl.uniform1i(LKeyPressedUniform_pv_ab, 0);
	    }
	}
	else if (shaderKeypress == 2)
	{
	    gl.useProgram(shaderProgramObject_pf_ab);

	    gl.uniformMatrix4fv(modelMatrixUniform_pf, false, modelMatrix);
	    gl.uniformMatrix4fv(viewMatrixUniform_pf, false, viewMatrix);
	    gl.uniformMatrix4fv(projectionMatrixUniform_pf, false, perspectiveProjectionMatrix);

	    if (lKeyPressed == true) {
	        gl.uniform1i(LKeyPressedUniform_pf_ab, 1);
	        gl.uniform3fv(laUniform_pf_ab, lightAmbient_ab);
	        gl.uniform3fv(ldUniform_pf_ab, lightDiffuse_ab);
	        gl.uniform3fv(lsUniform_pf_ab, lightSpecular_ab);
	        gl.uniform3fv(kaUniform_pf_ab, materialAmbient_ab);
	        gl.uniform3fv(kdUniform_pf_ab, materialDiffuse_ab);
	        gl.uniform3fv(ksUniform_pf_ab, materialSpecular_ab);
	        gl.uniform1f(materialShininessUniform_pf_ab, materialShininess_ab);
	        gl.uniform4fv(lightPositionUniform_pf_ab, lightPosition_ab);
	    }
	    else {
	        gl.uniform1i(LKeyPressedUniform_pf_ab, 0);
	    }
	}
	
    //Draw the sphere_ab
	sphere_ab.draw();

	gl.useProgram(null);

	update();

	//Animation Loop
	requestAnimationFrame(draw, canvas_ab);
}

function update()
{
    //Code
	
}

function uninitialize()
{
	//Code
	if(sphere_ab)
	{
	    sphere_ab.deallocate();
	    sphere_ab = null;
	}

	if (shaderProgramObject_pf_ab)
	{
		if(fragmentShaderObject_pf_ab)
		{
		    gl.detachShader(shaderProgramObject_pf_ab, fragmentShaderObject_pf_ab);
		    gl.deleteShader(fragmentShaderObject_pf_ab);
		    fragmentShaderObject_pf_ab = null;
		}

		if(vertexShaderObject_pf_ab)
		{
		    gl.detachShader(shaderProgramObject_pf_ab, vertexShaderObject_pf_ab);
		    gl.deleteShader(vertexShaderObject_pf_ab);
		    vertexShaderObject_pf_ab = null;
		}

		gl.deleteProgram(shaderProgramObject_pf_ab);
		shaderProgramObject_pf_ab = null;
	}

	if (shaderProgramObject_pv_ab)
	{
	    if (fragmentShaderObject_pv_ab)
	    {
	        gl.detachShader(shaderProgramObject_pv_ab, fragmentShaderObject_pv_ab);
	        gl.deleteShader(fragmentShaderObject_pv_ab);
	        fragmentShaderObject_pv_ab = null;
	    }

	    if (vertexShaderObject_pv_ab)
	    {
	        gl.detachShader(shaderProgramObject_pv_ab, vertexShaderObject_pv_ab);
	        gl.deleteShader(vertexShaderObject_pv_ab);
	        vertexShaderObject_pv_ab = null;
	    }

	    gl.deleteProgram(shaderProgramObject_pv_ab);
	    shaderProgramObject_pv_ab = null;
	}
}

function keyDown(event)
{
	//Code
	switch(event.keyCode)
	{

		case 27:
			//Escape for uninitialize
			uninitialize();
			//Close our Application Tab
			window.close();
			break;

		case 91: // For 'F1'
			toggleFullscreen();
			break;

	    case 86: // For 'V' of 'v'
	        shaderKeypress = 1;
	        break;

	    case 70: // For 'F' of 'f'
	        shaderKeypress = 2;
	        break;

	    case 76: // For 'L' or 'l'
	        if (lKeyPressed == false)
	        {
	            lKeyPressed = true;
	        }
	        else
	        {
	            lKeyPressed = false;
	        }
	        break;
	}
}

function mouseDown()
{
	//Code
}

function degToRad(degrees)
{
	return(degrees * Math.PI / 180);
}
