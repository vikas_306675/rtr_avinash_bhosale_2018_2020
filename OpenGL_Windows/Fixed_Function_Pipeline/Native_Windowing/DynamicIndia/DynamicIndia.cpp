#include<stdio.h>
#include<Windows.h>
#include<gl/GLU.h>
#include<gl/GL.h>
#define _USE_MATH_DEFINES 1
#include <math.h>
#include<Mmsystem.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "Winmm.lib")

//Global Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global Variable Declaration
HWND ghwnd;
WINDOWPLACEMENT wpPrev;
MONITORINFO mi = { sizeof(MONITORINFO) };
DWORD dwStyle;
FILE *gbFile = NULL;
HDC ghdc;
HGLRC ghrc;
bool gbActiveWindow = false;
bool gbDone = false;
bool gbFullscreen = false;
int winWidth = 1920;
int winHeight = 1080;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	int initialize(void);
	void display(void);

	//Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyOGLWindow");
	int iRet = 0;

	//Creating log file
	if (fopen_s(&gbFile, "Log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Failed to Create Log File"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gbFile, "Log File Creation is Successful\n");
	}

	//Code
	//Creating BluePrint of the Window
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Registering the Window Class
	RegisterClassEx(&wndclass);

	//Creating the Window in the Memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("My OpenGL Window - Avinash Bhosale"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		winWidth,
		winHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	if (hwnd != NULL)
	{
		fprintf(gbFile, "Window Created successfully\n");
	}

	//Storing the Window Handle into Global Handle
	ghwnd = hwnd;

	//Initializing OpenGL
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gbFile, "Choosing Pixel Format Failed!!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gbFile, "Setting Pixel Format Failed!!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gbFile, "Creating Context Failed!!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gbFile, "Make Current Failed!!\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gbFile, "Initialization is Successful!!\n");
	}

	//Showing the Created Window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	PlaySound(MAKEINTRESOURCE(101), NULL, SND_RESOURCE | SND_ASYNC | SND_NODEFAULT);

	//Game Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (msg.message == WM_QUIT)
			{
				gbDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//Call Update Here

			}

			display();
		}
	}

	return((int)msg.wParam);
}

//Callback Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void resize(int, int);
	void uninitialize(void);

	//Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		winWidth = LOWORD(lParam);
		winHeight = HIWORD(lParam);
		break;

	case WM_ERASEBKGND:
		return(0);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//Fullscreen Toggle
void FullscreenToggle(void)
{
	//Setting the Window to Fullscreen
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		GetWindowPlacement(ghwnd, &wpPrev);
		GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd,
			HWND_TOP,
			mi.rcMonitor.left,
			mi.rcMonitor.left,
			mi.rcMonitor.right - mi.rcMonitor.left,
			mi.rcMonitor.bottom - mi.rcMonitor.top,
			SWP_NOZORDER | SWP_FRAMECHANGED);
	}
	gbFullscreen = true;
	ShowCursor(FALSE);
}

//Initialize
int initialize(void)
{
	//Function Declaration
	void resize(int, int);
	void FullscreenToggle(void);

	//Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//Code
	//Initialization of PFD Structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = { sizeof(PIXELFORMATDESCRIPTOR) };
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghdc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	//Set the Color State of OpenGL
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Warm Up call for resize 
	resize(winWidth, winHeight);

	FullscreenToggle();

	return 0;
}

//Resize
void resize(int width, int height)
{
	//Code
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

//Display
void display(void)
{
	//Function Declaration
	void drawIL(void);
	void drawN(void);
	void drawD(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void drawIR(void);
	void drawA(void);
	void drawPlane(void);
	void drawExhaust(void);

	//Variable Declaration
	static GLfloat plane_x = -2.40f;
	static GLfloat translateILx = -2.0f;
	static GLfloat translateNy = 2.5f;
	static GLfloat translateIRy = -2.0f;
	static GLfloat translateAx = 2.0f;
	static GLfloat colorSaffron1 = 0.0f;
	static GLfloat colorSaffron2 = 0.0f;
	static GLfloat colorSaffron3 = 0.0f;
	static GLfloat colorGreen1 = 0.0f;
	static GLfloat colorGreen2 = 0.0f;
	static GLfloat colorGreen3 = 0.0f;
	static GLfloat plane_rotation = -90.0f;
	static GLsizei plane_radius = 0;
	static GLfloat exhaust_x1 = -0.75f, exhaust_x2 = -0.70f;
	GLsizei i = 1000;
	GLfloat plane_angle = (M_PI / 2) * plane_radius / i;
	static GLfloat plane_x2 = 0.0f;
	static GLsizei counter = 0.0f;

	//Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//Bringing I from Left
	glTranslatef(translateILx, 0.0f, -3.0f);
	glLineWidth(5.0f);
	drawIL();

	//Bringing N from Top
	glLoadIdentity();
	glTranslatef(0.0f, translateNy, -3.0f);
	drawN();
	
	//Fading In the Letter D using Color Variables
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	drawD(colorSaffron1, colorSaffron2, colorSaffron3, colorGreen1, colorGreen2, colorGreen3);

	//Bringing I from the Bottom
	glLoadIdentity();
	glTranslatef(0.0f, translateIRy, -3.0f);
	drawIR();

	//Bringing A from the Right
	glLoadIdentity();
	glTranslatef(translateAx, 0.0f, -3.0f);
	drawA();

	//Bringing the Horizontal Plane from Left side
	glLoadIdentity();
	glTranslatef(plane_x, 0.0f, -2.0f);
	drawPlane();

	//Drawing the Exhaust of the Tri Color
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -2.0f);
	if (plane_x >= -0.60f)
	{

		glLineWidth(5.0f);
		glBegin(GL_LINES);

		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(exhaust_x1, 0.01f, 0.0f);
		glColor3f(1.00f, 0.60f, 0.20f);
		glVertex3f(exhaust_x2, 0.01f, 0.0f);

		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(exhaust_x1, 0.0f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(exhaust_x2, 0.0f, 0.0f);

		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(exhaust_x1, -0.01f, 0.0f);
		glColor3f(0.07f, 0.53f, 0.027f);
		glVertex3f(exhaust_x2, -0.01f, 0.0f);
		glEnd();
	}

	//Bringing in the Planes from Top and Bottom Left corners and rotating them on Z axis
	glLoadIdentity();
	glTranslatef(-0.6f, 1.0f, 0.0f);
	glTranslatef(-cos(plane_angle) + plane_x2, -sin(plane_angle), -2.0f);
	glRotatef(plane_rotation, 0.0f, 0.0f, 1.0f);
	drawPlane();

	glLoadIdentity();
	glTranslatef(-0.6f, -1.0f, 0.0f);
	glTranslatef(-cos(plane_angle) + plane_x2, sin(plane_angle), -2.0f);
	glRotatef(-plane_rotation, 0.0f, 0.0f, 1.0f);
	drawPlane();

	//Swapping Buffers
	SwapBuffers(ghdc);

	//Condition to translate I coming in from Left side
	if (translateILx <= 0.0f)
	{
		translateILx = translateILx + 0.005f;
	}

	//Condition to translate A coming in from Right side
	if (translateILx >= 0.0f)
	{
		if (translateAx >= 00.f)
		{
			translateAx = translateAx - 0.005f;
		}
	}

	//Condition to translate N coming in from Top side
	if (translateAx <= 0.0f)
	{
		if (translateNy >= 0.0f)
		{
			translateNy = translateNy - 0.005f;
		}
	}

	//Condition to translate I coming in from Bottom side
	if (translateNy <= 0.0f)
	{
		if (translateIRy <= 0.0f)
		{
			translateIRy = translateIRy + 0.005f;
		}
	}

	//Condition to Fade In D using Color variables
	if (translateIRy >= 0.0f)
	{
		if (colorSaffron1 <= 1.0f)
		{
			colorSaffron1 = colorSaffron1 + 0.002f;
		}

		if (colorSaffron2 <= 0.60f)
		{
			colorSaffron2 = colorSaffron2 + 0.001f;
		}

		if (colorSaffron3 <= 0.20f)
		{
			colorSaffron3 = colorSaffron3 + 0.003f;
		}

		if (colorGreen1 <= 0.07f)
		{
			colorGreen1 = colorGreen1 + 0.010f;
		}

		if (colorGreen2 <= 0.53f)
		{
			colorGreen2 = colorGreen2 + 0.002f;
		}

		if (colorGreen3 <= 0.027f)
		{
			colorGreen3 = colorGreen3 + 0.010f;
		}
	}

	//Condition to translate the plane coming in Horizontally
	if (colorSaffron1 >= 1.0f)
	{
		if (plane_x <= 0.65f)
		{
			plane_x = plane_x + 0.00361f;
		}

		if (plane_radius < i)
		{
			plane_radius = plane_radius + 2;
			if (plane_rotation <= 0.0f)
			{
				plane_rotation = plane_rotation + 0.2f;
			}
		}

		if (counter <= 3400)
		{
			counter = counter + 1;
		}
	}

	//Condition to translate the Planes coming in from an angle to move Horizontally after the angle ends on the x-axis
	if (plane_radius >= i)
	{
		if (plane_x <= 0.65f)
		{
			plane_x2 = plane_x2 + 0.00361f;
		}
	}

	if (counter >= 3400)
	{
		
		if (plane_x <= 2.0f)
		{
			plane_x = plane_x + 0.00361f;
		}

		if (plane_radius < 2 * i)
			{
				plane_radius = plane_radius + 2;
			}

		if (plane_x > 0.65f)
		{
				plane_x2 = plane_x2 + 0.00005f;
		}

		plane_rotation = plane_rotation + 0.2f;

	}
	//Condition to start drawing the exhaust of Tri Color
	if (plane_x >= -0.60f)
	{
		if (exhaust_x2 <= 0.43f)
		{
			exhaust_x2 = exhaust_x2 + 0.00361f;
		}
	}

	//Condition to Fade out the Tri Color to a certain point on A
	if (exhaust_x2 >= 0.43f)
	{
		if (exhaust_x1 <= 0.34f)
		{
			exhaust_x1 = exhaust_x1 + 0.00361f;
		}
	}
}

//Draw Left I
void drawIL(void)
{
	//Drawing Left I of INDIA
	glBegin(GL_LINES);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.60f, 0.60f, 0.0f);
	glVertex3f(-0.80f, 0.60f, 0.0f);
	glVertex3f(-0.70f, 0.60f, 0.0f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(-0.70f, -0.60f, 0.0f);
	glVertex3f(-0.80f, -0.60f, 0.0f);
	glVertex3f(-0.60f, -0.60f, 0.0f);
	glEnd();
}

//Draw N
void drawN(void)
{
	//Drawing N of INDIA
	glBegin(GL_LINES);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.50f, 0.60f, 0.00f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(-0.50f, -0.60f, 0.0f);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.50f, 0.60f, 0.00f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(-0.30f, -0.60f, 0.0f);
	glVertex3f(-0.30f, -0.60f, 0.0f);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.30f, 0.60f, 0.0f);
	glEnd();
}

//Draw D
void drawD(GLfloat color1, GLfloat color2, GLfloat color3, GLfloat color4, GLfloat color5, GLfloat color6)
{
	//Drawing D of INDIA
	glBegin(GL_LINES);
	glColor3f(color1, color2, color3);
	glVertex3f(-0.10f, 0.60f, 0.00f);
	glColor3f(color4, color5, color6);
	glVertex3f(-0.10f, -0.60f, 0.0f);
	glColor3f(color1, color2, color3);
	glVertex3f(-0.15f, 0.60f, 0.00f);
	glVertex3f(0.10f, 0.60f, 0.00f);
	glVertex3f(0.10f, 0.60f, 0.00f);
	glColor3f(color4, color5, color6);
	glVertex3f(0.10f, -0.60f, 0.0f);
	glVertex3f(0.10f, -0.60f, 0.0f);
	glVertex3f(-0.15f, -0.60f, 0.00f);
	glEnd();
}

//Draw Right R
void drawIR(void)
{
	//Drawing Right I of INDIA
	glBegin(GL_LINES);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(0.20f, 0.60f, 0.0f);
	glVertex3f(0.40f, 0.60f, 0.0f);
	glVertex3f(0.30f, 0.60f, 0.0f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(0.30f, -0.60f, 0.0f);
	glVertex3f(0.20f, -0.60f, 0.0f);
	glVertex3f(0.40f, -0.60f, 0.0f);
	glEnd();
}

//Draw A
void drawA(void)
{
	//Drawing A of INDIA
	glBegin(GL_LINES);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(0.70f, -0.60f, 0.0f);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(0.60f, 0.60f, 0.0f);
	glVertex3f(0.60f, 0.60f, 0.0f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(0.50f, -0.60f, 0.0f);
	glEnd();
}

//Draw Planes
void drawPlane(void)
{
	glBegin(GL_TRIANGLES);
	glColor3f(0.726f, 0.882f, 0.929f);
	glVertex3f(0.0f, 0.025f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glEnd();
	glBegin(GL_TRIANGLES);
	glColor3f(0.726f, 0.882f, 0.929f);
	glVertex3f(0.0f, -0.025f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glEnd();
	glBegin(GL_TRIANGLES);
	glColor3f(0.726f, 0.882f, 0.929f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(-0.075f, 0.025f, 0.0f);
	glVertex3f(-0.075f, -0.025f, 0.0f);
	glBegin(GL_TRIANGLES);
	glColor3f(0.726f, 0.882f, 0.929f);
	glVertex3f(0.00f, -0.05f, 0.0f);
	glVertex3f(-0.025f, 0.0f, 0.0f);
	glVertex3f(0.025f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.05f, 0.0f);
	glVertex3f(-0.025f, 0.0f, 0.0f);
	glVertex3f(0.025f, 0.0f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.010f, 0.013f, 0.0f);
	glVertex3f(-0.010f, -0.013f, 0.0f);

	glVertex3f(0.002f, -0.013f, 0.0f);
	glVertex3f(-0.001f, 0.013f, 0.0f);
	glVertex3f(-0.001f, 0.013f, 0.0f);
	glVertex3f(-0.005f, -0.013f, 0.0f);

	glVertex3f(0.012f, 0.013f, 0.0f);
	glVertex3f(0.008f, 0.013f, 0.0f);
	glVertex3f(0.011f, 0.004f, 0.0f);
	glVertex3f(0.008f, 0.004f, 0.0f);
	glVertex3f(0.008f, 0.013f, 0.0f);
	glVertex3f(0.008f, -0.013f, 0.0f);
	glEnd();
}

//Unintialize
void uninitialize(void)
{
	/*if (gbFullscreen = true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
	}*/
	ShowCursor(TRUE);

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gbFile)
	{
		fprintf(gbFile, "Log File Closed Successfully!!\n");
		fclose(gbFile);
		gbFile = NULL;
	}
}