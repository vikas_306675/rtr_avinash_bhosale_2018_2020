#include<stdio.h>
#include<conio.h>

int main(void)
{
	//Variable Declaration
	int a, b, result;
	char option, option_division;

	//Code
	printf("\nEnter a Value for 'A': ");
	scanf("%d", &a);

	printf("\nEnter a Value for 'B': ");
	scanf("%d", &b);

	printf("\nEnter an option in the form of a Character:");
	printf("\n'A' or 'a' for Addition.");
	printf("\n'S' or 's' for Subtraction.");
	printf("\n'M' or 'm' for Multiplication.");
	printf("\n'D' or 'd' for Division");
	printf("\nEnter an option from the above: ");
	option = getch();

	printf("\n");

	if (option == 'A' || option == 'a')
	{
		result = a + b;
		printf("\nAddition Of A = %d And B = %d gives result %d.\n", a, b, result);
	}
	else if (option == 'S' || option == 's')
	{
		if (a >= b)
		{
			result = a - b;
			printf("\nSubtraction Of B = %d from A = %d gives result %d.\n", b, a, result);
		}
		else
		{
			result = b - a;
			printf("\nSubtraction Of A = %d from B = %d gives result %d.\n", a, b, result);
		}
	}
	else if (option == 'M' || option == 'm')
	{
		result = a * b;
		printf("\nMultiplication Of A = %d and B = %d gives result %d.\n", a, b, result);
	}
	else if (option == 'D' || option == 'd')
	{
		printf("\nEnter an Option in the form of a Character:");
		printf("\n'Q' or 'q' or '/' for Quotient upon Division.");
		printf("\n'R' or 'r' or '%%' for Remainder upon Division.");

		printf("\nEnter an Option: ");
		option_division = getch();

		printf("\n");

		if (option_division == 'Q' || option_division == 'q' || option_division == '/')
		{
			if (a >= b)
			{
				result = a / b;
				printf("\nDivision Of A = %d by B = %d gives Quotient = %d.\n", a, b, result);
			}
			else
			{
				result = b / a;
				printf("\nDivision Of B = %d by A = %d gives Quotient = %d.\n", b, a, result);
			}
		}
		else if (option_division == 'R' || option_division == 'r' || option_division == '%')
		{
			if (a >= b)
			{
				result = a % b;
				printf("\nDivision Of A = %d by B = %d gives Remainder = %d.\n", a, b, result);
			}
			else
			{
				result = b / a;
				printf("\nDivision Of B = %d by A = %d gives Remainder = %d.\n", b, a, result);
			}
		}
		else
			printf("\nInvalid character %c entered for Division!!\n", option_division);
	}
	else
		printf("\nInvalid character %c entered!!\n", option);

	printf("\nIf-Else If ladder Complete.\n");

	return(0);
}
