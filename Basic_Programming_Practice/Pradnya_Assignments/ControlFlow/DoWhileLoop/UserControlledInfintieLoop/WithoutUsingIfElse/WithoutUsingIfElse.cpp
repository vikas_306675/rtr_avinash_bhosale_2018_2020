#include<stdio.h>
#include<conio.h>

int main(void)
{
	//Variable Declaration
	char option, ch = '\0';

	//Code
	printf("\nOnce the Infine Loop begins, Press 'Q' or 'q' to Quit the infine loop.\n");
	
	do
	{
		do
		{
			printf("\nIn Loop...");
			ch = getch();
			if (ch == 'Q' || ch == 'q')
				break; //Get out of the loop
		} while (1);
		
		printf("\nExiting the infine loop..\n");
		printf("\nDo you want to begin the infinite loop again? (Press Y/y for Yes, Any other key to quit.\n");
		option = getch();
	} while (option == 'Y' || option == 'y');

	printf("\nProgram ends here!!\n");

	return 0;
}