#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j, c;

	//Code
	printf("\n");
	i = 0;
	while (i < 64)
	{
		j = 0;
		while (j < 64)
		{
			c = ((i & 0x8) == 0) ^ ((j & 0x8) == 0);

			if (c == 0)
				printf("  ");

			if (c == 1)
				printf("* ");

			j++;
		}
		i++;
		printf("\n");
	}

	printf("\nCheckered Board is Complete.\n");

	return 0;
}