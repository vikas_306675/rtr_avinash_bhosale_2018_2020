#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i;

	//Code
	printf("\nPrinting Digits from 10 to 1 - ");

	i = 10;
	do
	{
		printf("\n%d", i);
		i--;
	}while (i > 0);

	printf("\nDo-While Loop Completed.\n");

	return 0;
}