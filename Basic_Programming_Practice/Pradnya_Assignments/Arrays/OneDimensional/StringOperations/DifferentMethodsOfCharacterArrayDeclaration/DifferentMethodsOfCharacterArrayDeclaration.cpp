#include <stdio.h>

int main(void)
{
	//Variable Declarations
	char chArray_01[] = { 'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P', '\0' };
	char chArray_02[9] = { 'W', 'E', 'L', 'C', 'O', 'M', 'E', 'S', '\0' };
	char chArray_03[] = { 'Y', 'O', 'U', '\0' };
	char chArray_04[] = "To"; 
	char chArray_05[] = "REAL TIME RENDERING BATCH OF 2018-19-20";

	char chArray_WithoutNullTerminator[] = { 'H', 'e', 'l', 'l', 'o' };

	///Code
	printf("\nSize of chArray_01: %d\n", sizeof(chArray_01));
	printf("\nSize of chArray_02: %d\n", sizeof(chArray_02));
	printf("\nSize of chArray_03: %d\n", sizeof(chArray_03));
	printf("\nSize of chArray_04: %d\n", sizeof(chArray_04));
	printf("\nSize of chArray_05: %d\n", sizeof(chArray_05));

	printf("\nThe Strings are: \n");
	printf("\nchArray_01: %s\n", chArray_01);
	printf("\nchArray_02: %s\n", chArray_02);
	printf("\nchArray_03: %s\n", chArray_03);
	printf("\nchArray_04: %s\n", chArray_04);
	printf("\nchArray_05: %s\n", chArray_05);

	printf("\nSize Of chArray_WithoutNullTerminator: %d\n", sizeof(chArray_WithoutNullTerminator));
	printf("\nchArray_WithoutNullTerminator: %s\n", chArray_WithoutNullTerminator); //As \0 is not present at the end this displays garbage value at the end of the string

	return 0;
}

