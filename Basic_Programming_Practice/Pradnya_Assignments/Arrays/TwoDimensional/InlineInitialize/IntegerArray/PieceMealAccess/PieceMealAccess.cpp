#include <stdio.h>

int main(void)
{
	//Variable Declaraions
	int iArray[5][3] = { {1, 2, 3}, {2, 4, 6}, {3, 6, 9}, {4, 8, 12}, {5, 10, 15} };
	int int_size, iArray_size, iArray_num_elements, iArray_num_rows, iArray_num_columns;

	//Code
	int_size = sizeof(int);

	iArray_size = sizeof(iArray);
	printf("\nSize of Two Dimensional Integer Array = %d\n", iArray_size);

	iArray_num_rows = iArray_size / sizeof(iArray[0]);
	printf("\nNumber of Rows in Two Dimensional Integer Array = %d\n", iArray_num_rows);

	iArray_num_columns = sizeof(iArray[0]) / int_size;
	printf("\nNumber of Columns in Two Dimensional Integer Array = %d\n", iArray_num_columns);

	iArray_num_elements = iArray_num_rows * iArray_num_columns;
	printf("\nNumber of Elements in Two Dimensional Integer Array = %d\n", iArray_num_elements);

	printf("\nElements in the 2D Array: \n");

	printf("\n****** ROW 1 ******\n");
	printf("iArray[0][0] = %d\n", iArray[0][0]); 
	printf("iArray[0][1] = %d\n", iArray[0][1]); 
	printf("iArray[0][2] = %d\n", iArray[0][2]);

	printf("\n****** ROW 2 ******\n");
	printf("iArray[1][0] = %d\n", iArray[1][0]); 
	printf("iArray[1][1] = %d\n", iArray[1][1]);
	printf("iArray[1][2] = %d\n", iArray[1][2]);

	printf("\n****** ROW 3 ******\n");
	printf("iArray[2][0] = %d\n", iArray[2][0]); 
	printf("iArray[2][1] = %d\n", iArray[2][1]);
	printf("iArray[2][2] = %d\n", iArray[2][2]);

	printf("\n****** ROW 4 ******\n");
	printf("iArray[3][0] = %d\n", iArray[3][0]);
	printf("iArray[3][1] = %d\n", iArray[3][1]);
	printf("iArray[3][2] = %d\n", iArray[3][2]);

	printf("\n****** ROW 5 ******\n");
	printf("iArray[4][0] = %d\n", iArray[4][0]);
	printf("iArray[4][1] = %d\n", iArray[4][1]);
	printf("iArray[4][2] = %d\n", iArray[4][2]);

	return 0;
}

