#include <stdio.h>
#include <conio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char ch;
};

int main(void)
{
	//Varibale Declarations
	struct MyData data;

	//Code
	printf("\nEnter Integer-Value for Data Member 'i' of 'struct MyData': \n");
	scanf("%d", &data.i);

	printf("Enter Floating-Point Value for Data Member 'f' Of 'struct MyData': \n");
	scanf("%f", &data.f);

	printf("Enter Double-Value for Data Member 'd' Of 'struct MyData': \n");
	scanf("%lf", &data.d);

	printf("Enter Character-Value for Data Member 'c' Of 'struct MyData': \n");
	data.ch = getch();

	printf("\nDATA MEMBERS of 'struct MyData': \n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);
	printf("c = %c\n\n", data.ch);

	return 0;
}
