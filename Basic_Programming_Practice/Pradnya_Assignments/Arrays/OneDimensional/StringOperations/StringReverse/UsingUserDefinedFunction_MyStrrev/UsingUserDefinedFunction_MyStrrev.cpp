#include <stdio.h>
#include<string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declaration
	void MyStrrev(char[], char[]);

	//Variable Declarations 
	char chArray_Original[MAX_STRING_LENGTH], chArray_Reversed[MAX_STRING_LENGTH];

	//Code
	printf("\nEnter a String: \n");
	gets_s(chArray_Original, MAX_STRING_LENGTH);

	MyStrrev(chArray_Reversed, chArray_Original);

	printf("\nThe Original String Entered ('chArray_Original[]'): \n");
	printf("%s\n", chArray_Original);

	printf("\nReversed String ('chArray_Reversed[]'): \n");
	printf("%s\n", chArray_Reversed);

	return 0;
}


//Reverse String Function
void MyStrrev(char str_destination[], char str_source[])
{
	//Function Declaration
	int MyStrlen(char[]);

	//Variable Declarations 
	int iStringLength = 0, i, j, len;

	//Code
	iStringLength = MyStrlen(str_source);

	len = iStringLength - 1;

	for (i = 0, j = len; i < iStringLength, j >= 0; i++, j--)
	{
		str_destination[i] = str_source[j];
	}

	str_destination[i] = '\0';
}

//Function to calculate string length
int MyStrlen(char str[])
{
	//Variable Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return(string_length);
}
