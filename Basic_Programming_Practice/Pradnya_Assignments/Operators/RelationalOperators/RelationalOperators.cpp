#include<stdio.h>

int main(void)
{
	//variable declaration
	int a, b, result;

	//code
	printf("\nEnter a number: ");
	scanf("%d", &a);

	printf("\nEnter another number: ");
	scanf("%d", &b);

	printf("\nIf Answer = 0, it is FALSE and if Answer = 1, it is TRUE");

	result = (a > b);
	printf("\nThe result of (%d > %d) is %d", a, b, result);

	result = (a < b);
	printf("\nThe result of (%d < %d) is %d", a, b, result);

	result = (a >= b);
	printf("\nThe result of (%d >= %d) is %d", a, b, result);

	result = (a <= b);
	printf("\nThe result of (%d <= %d) is %d", a, b, result);

	result = (a == b);
	printf("\nThe result of (%d == %d) is %d", a, b, result);

	result = (a != b);
	printf("\nThe result of (%d != %d) is %d", a, b, result);

	return 0;
}